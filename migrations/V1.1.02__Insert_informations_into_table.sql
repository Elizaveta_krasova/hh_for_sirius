-- Сиды для таблиц users
insert into users (email, password_hash, role) values
-- везде пароль admin
('student@example.com', '$2b$10$NxeZ0HJtkviAqF3KGlobauY3P4rDjz06KwyZ2dSMT507S5t.Fv3rW', 'student'),
('student2@example.com', '$2b$10$NxeZ0HJtkviAqF3KGlobauY3P4rDjz06KwyZ2dSMT507S5t.Fv3rW', 'student'),
('company@example.com', '$2b$10$NxeZ0HJtkviAqF3KGlobauY3P4rDjz06KwyZ2dSMT507S5t.Fv3rW', 'company'),
('company2@example.com', '$2b$10$NxeZ0HJtkviAqF3KGlobauY3P4rDjz06KwyZ2dSMT507S5t.Fv3rW', 'company'),
('admin@example.com', '$2b$10$NxeZ0HJtkviAqF3KGlobauY3P4rDjz06KwyZ2dSMT507S5t.Fv3rW', 'admin');

-- Сиды для таблиц directions
insert into directions (title, description) values
('IT', 'Information Technology'),
('Engineering', 'Engineering and Construction');

-- Сиды для таблиц groups
insert into groups (direction_id, title, years_of_study, education_start) values
((select id from directions where title = 'IT'), 'К0711-22/1', 4, 2022),
((select id from directions where title = 'Engineering'), 'К0111-21/1', 3, 2023);

-- Сиды для таблиц students
insert into students (user_id, group_id, name, middle_name, lastname, phone, birthday, telegram, average_score, status) values
((select id from users where email = 'student@example.com'), (select id from groups where years_of_study = 4), 'John', 'Doe', 'Smith', '123456789', '1998-05-20', '@john_doe', 4.5, 'job'),
((select id from users where email = 'student2@example.com'), (select id from groups where years_of_study = 3), 'Alice', 'Johnson', 'Brown', '987654321', '1999-08-15', '@alice', 4.0, 'internship');

-- Сиды для таблиц resumes
insert into resumes (about, student_id) values
('Experienced in Java programming', (select id from students where name = 'John')),
('Skills in project management', (select id from students where name = 'Alice'));

-- Сиды для таблиц projects
insert into projects (resume_id, date_from, date_to, title, description) values
((select id from resumes where about = 'Experienced in Java programming'), '2022-01-10', '2023-01-10', 'Java Development Project', 'Developed backend systems for a tech company'),
((select id from resumes where about = 'Experienced in Java programming'), '2022-01-10', '2022-02-10', 'C# Development Project', 'Developed backend systems for a tech company'),
((select id from resumes where about = 'Skills in project management'), '2022-02-15', '2022-04-15', 'Project Management Initiative', 'Led a team in implementing a new work process');

-- Сиды для таблиц companies
insert into companies (user_id, title, industry, address, link_to_web_site, logo) values
((select id from users where email = 'company@example.com'), 'Tech Solutions Ltd', 'Technology', '123 Tech Street', 'https://techsolutions.com', 'tech_logo.png'),
((select id from users where email = 'company2@example.com'), 'BuildIt Construction', 'Construction', '456 Construction Avenue', 'https://buildit.com', 'buildit_logo.png');

-- Сиды для таблиц vacancies
insert into vacancies (company_id, title, description, type, date_from, date_to, expiration, task, city, is_remote) values
((select id from companies where title = 'Tech Solutions Ltd'), 'Software Developer Intern', 'Join our team as a software developer intern', 'internship', '2022-06-01', '2022-09-01', '2022-05-15', 'Develop a new feature for our app', 'New York', false),
((select id from companies where title = 'Tech Solutions Ltd'), 'C# Developer Intern', 'Join our team as a software developer intern', 'internship', '2022-06-01', '2022-09-01', '2022-05-15', 'Develop a new feature for our app', 'Sochi', false),
((select id from companies where title = 'BuildIt Construction'), 'Project Manager Position', 'Exciting opportunity for a project manager', 'job', '2022-07-01', '2023-07-01', '2022-06-01', 'Lead a construction project', 'Chicago', true);

-- Сиды для таблиц applies
insert into applies (vacancy_id, student_id, task, is_applied) values
((select id from vacancies where title = 'Software Developer Intern'), (select id from students where name = 'John'), 'Develop a new feature for our app', true),
((select id from vacancies where title = 'Project Manager Position'), (select id from students where name = 'Alice'), 'Lead a construction project', false);

-- Сиды для таблиц tags
insert into tags (title) values
('Java'),
('Project Management');

-- Сиды для таблиц student_to_tag
insert into student_to_tag (tag_id, student_id) values
((select id from tags where title = 'Java'), (select id from students where name = 'John')),
((select id from tags where title = 'Project Management'), (select id from students where name = 'Alice'));

-- Сиды для таблиц vacancy_to_tag
insert into vacancy_to_tag (tag_id, vacancy_id) values
((select id from tags where title = 'Java'), (select id from vacancies where title = 'Software Developer Intern')),
((select id from tags where title = 'Project Management'), (select id from vacancies where title = 'Project Manager Position'));