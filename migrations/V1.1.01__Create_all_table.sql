CREATE EXTENSION "uuid-ossp";
drop type if exists role, vacancy_type, status cascade;
drop table if exists users, directions, groups, resumes, projects, students,
    vacancies, companies, applies, tags, student_to_tag, vacancy_to_tag cascade;

create type role as enum ('student', 'company', 'admin');
create type vacancy_type as enum ('practice', 'internship', 'job');
create type status as enum ('practice', 'internship', 'job', 'not_looking_for_job');

create table users
(
    id            uuid          default uuid_generate_v4() primary key,
    email         text not null unique,
    password_hash text not null,
    role          role not null default 'student'
);

create table directions
(
    id          uuid default uuid_generate_v4() primary key,
    title       text not null,
    description text not null
);

create table groups
(
    id              uuid default uuid_generate_v4() primary key,
    title           text not null,
    direction_id    uuid not null references directions,
    years_of_study  int  not null,
    education_start int  not null
);

create table students
(
    id            uuid default uuid_generate_v4() primary key,
    user_id       uuid    not null references users unique,
    group_id      uuid references groups,
    link_to_git   text,
    name          text    not null,
    middle_name   text    not null,
    lastname      text,
    phone         text    not null,
    photo         text,
    birthday      date    not null,
    telegram      text    not null,
    average_score numeric,
    status        status  not null,
    is_approved   boolean not null default false
);

create table resumes
(
    id         uuid default uuid_generate_v4() primary key,
    about      text not null,
    student_id uuid not null references students unique
);

create table projects
(
    id          uuid default uuid_generate_v4() primary key,
    resume_id   uuid not null references resumes,
    date_from   date not null,
    date_to     date,
    title       text not null,
    description text not null
);

create table companies
(
    id               uuid default uuid_generate_v4() primary key,
    user_id          uuid not null references users unique,
    title            text not null,
    industry         text not null,
    address          text,
    link_to_web_site text,
    logo             text
);

create table vacancies
(
    id          uuid default uuid_generate_v4() primary key,
    company_id  uuid         not null references companies,
    title       text         not null,
    description text         not null,
    type        vacancy_type not null,
    date_from   date         not null,
    date_to     date         not null,
    expiration  date         not null,
    task        text,
    city        text         not null,
    is_remote   boolean      not null
);

create table applies
(
    id         uuid default uuid_generate_v4() primary key,
    vacancy_id uuid not null references vacancies,
    student_id uuid not null references students,
    task       text,
    is_applied boolean,
    unique (vacancy_id, student_id)
);

create table tags
(
    id    uuid default uuid_generate_v4() primary key,
    title text not null
);

create table student_to_tag
(
    tag_id     uuid not null references tags,
    student_id uuid not null references students,
    unique (tag_id, student_id)
);

create table vacancy_to_tag
(
    tag_id     uuid not null references tags,
    vacancy_id uuid not null references vacancies,
    unique (tag_id, vacancy_id)
);