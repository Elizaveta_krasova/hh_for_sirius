export { default } from 'next-auth/middleware';

export const config = {
    // https://nextjs.org/docs/app/building-your-application/routing/middleware#matcher
    // роуты, которые редиректят в login, если юзер не аутентифицирован
    matcher: ['/applies/:path*', '/groups/:path*', '/vacancies/:path*','/directions/:path*','/companies/:path*', '/resumes/:path*', '/profile/student/:path*', '/profile/company/:path*'],
};
