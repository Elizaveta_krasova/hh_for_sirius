'use client';
import { useSession } from '@/app/lib/client-utils';
import { EUserRole } from '@/app/lib/definitions';

const useUser = () => {
  const session = useSession();
  return {
    user: session?.user,
    companyId: session?.user?.role === EUserRole.COMPANY ? (session?.user.company?.id as string) : '',
    studentId: session?.user?.role === EUserRole.STUDENT ? (session?.user.student?.id as string) : '',
    studentResumeId: session?.user?.role === EUserRole.STUDENT ? (session?.user.student?.resume_id as string) : '',
    userId: session?.user.id as string,
    isAdmin: session?.user?.role === EUserRole.ADMIN,
    isStudent: session?.user?.role === EUserRole.STUDENT,
    isCompany: session?.user?.role === EUserRole.COMPANY,
  };
};
export default useUser;
