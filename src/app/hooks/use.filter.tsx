'use client';

import { useEffect, useState } from 'react';

export default function useFilter<T>(initArr: Array<T>, filterBy: string, filterValue: string | 'all') {
  const [filteredArr, setFilteredArr] = useState<T[]>(initArr);

  useEffect(() => {
    if (filterBy && filterValue) {
      if (filterValue === 'all') {
        setFilteredArr(initArr);
      } else {
        // @ts-ignore
        setFilteredArr(initArr.filter((i) => i[filterBy] === filterValue));
      }
    }
  }, [filterBy, initArr, filterValue]);

  return { filteredArr, initArr, filterBy };
}
