import { Tag } from '@/app/lib/definitions';

export type WithTag<T> = T & { tags?: Tag[] };
export type PropsWithClassName<T> = T & { className?: string };
