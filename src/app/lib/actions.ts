'use server';

import {
  createApply,
  createCompany,
  createDirection,
  createGroup,
  createProject,
  createResume,
  createStudent,
  createTag,
  createVacancy,
  deleteDirection,
  deleteGroup,
  deleteProject,
  deleteTag,
  getResumeByStudentId,
  getTagByVacancyId,
  getTags,
  updateApply,
  updateCompany,
  updateDirection,
  updateGroup,
  updateProject,
  updateResume,
  updateStudent,
  updateStudentApproved,
  updateStudentAverageScore,
  updateStudentGroup,
  updateStudentToTag,
  updateTag,
  updateVacancy,
} from './data';
import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';
import {
  Vacancy,
  IVacancyCreate,
  IDirectionCreate,
  IGroupCreate,
  Group,
  IApplyCreate,
  Tag,
  Student,
  Project,
  IProjectCreate,
  Company,
} from '@/app/lib/definitions';
import Projects from '@/app/profile/student/projects';
import { routes } from '@/app/config/routes';

export async function createVacancyAction(values: IVacancyCreate) {
  revalidatePath(routes.vacancies.path);
  return await createVacancy(values);
}

export async function updateVacancyAction(id: string, values: Vacancy) {
  revalidatePath(routes.vacancies.path);
  return await updateVacancy(id, values);
}

export async function updateStudentToTagAction(id: string, values: Tag[]) {
  revalidatePath(routes.profile.student.path);
  return await updateStudentToTag(id, values);
}

export async function updateStudentGroupAction(id: string, group_id: string) {
  revalidatePath(routes.applies.students.path);
  return await updateStudentGroup(id, group_id);
}

export async function updateStudentAverageScoreAction(id: string, score: number) {
  revalidatePath(routes.applies.students.path);
  return await updateStudentAverageScore(id, score);
}

export async function updateStudentApprovedAction(id: string, values: boolean) {
  revalidatePath(routes.applies.students.path);
  return await updateStudentApproved(id, values);
}

export async function updateStudentAction(values: Student) {
  revalidatePath(routes.profile.student.path);
  return await updateStudent(values);
}

export async function updateCompanyAction(values: Company) {
  revalidatePath(routes.profile.company.path);
  return await updateCompany(
    values.id,
    values.title as string,
    values.industry as string,
    values.address as string,
    values.link_to_web_site as string,
    values.logo as string
  );
}

export async function createGroupAction(values: IGroupCreate) {
  revalidatePath(routes.groups.path);
  return await createGroup(values);
}

export async function updateGroupAction(id: string, values: Group) {
  revalidatePath(routes.groups.path);
  return await updateGroup(id, values);
}

export async function createDirectionAction(values: IDirectionCreate) {
  revalidatePath(routes.directions.path);
  await createDirection(values);
}

export async function deleteDirectionAction(id: string) {
  revalidatePath(routes.directions.path);
  await deleteDirection(id);
  redirect(routes.directions.path);
}

export async function deleteGroupAction(id: string) {
  revalidatePath(routes.groups.path);
  await deleteGroup(id);
  redirect(routes.groups.path);
}

export async function deleteTagAction(id: string) {
  revalidatePath(routes.tags.path);
  await deleteTag(id);
  redirect(routes.tags.path);
}

export async function updateDirectionAction(id: string, values: IDirectionCreate) {
  revalidatePath(routes.directions.path);
  return await updateDirection(id, values);
}

export async function createTagAction(title: string) {
  revalidatePath(routes.tags.path);
  return await createTag(title as string);
}

export async function createStudentAction({
  email,
  password,
  name,
  middleName,
  lastname,
  linkToGit,
  phone,
  birthday,
  telegram,
  status,
}: {
  email: string;
  password: string;
  name: string;
  middleName: string;
  lastname: string;
  linkToGit: string;
  phone: string;
  birthday: any;
  telegram: string;
  status: string;
}) {
  return await createStudent(email, password, name, middleName, lastname, linkToGit, phone, birthday, telegram, status);
}

export async function getTagsAction() {
  return await getTags();
}

export async function getTagByVacancyIdAction(id: string) {
  return await getTagByVacancyId(id);
}

export async function updateTagAction(values: Tag) {
  revalidatePath(routes.tags.path);
  return await updateTag(values.id, values.title as string);
}

export async function createProjectAction(resumeId: string, values: IProjectCreate) {
  revalidatePath(routes.profile.student.path);
  return await createProject(resumeId as string, values.date_from, values.date_to, values.title as string, values.description as string);
}

export async function updateProjectAction(values: Project) {
  revalidatePath(routes.profile.student.path);
  return await updateProject(values.id, values.date_from, values.date_to, values.title as string, values.description as string);
}

export async function deleteProjectAction(id: string) {
  revalidatePath(routes.profile.student.path);
  return await deleteProject(id);
}

export async function createApplyAction(values: IApplyCreate) {
  revalidatePath(routes.vacancies.path);
  return await createApply(values);
}

export async function updateApplyAction(id: string, vacancyId: string, isApplied: boolean) {
  await updateApply(id, Boolean(isApplied));

  revalidatePath(routes.applies.vacancy.generatePath(vacancyId));
  redirect(routes.applies.vacancy.generatePath(vacancyId));
}

export async function createResumeAction(studentId: string, about: string) {
  revalidatePath(routes.profile.student.path);
  return await createResume(studentId, about);
}

export async function createCompanyAction(values: {
  email: string;
  password: string;
  title: string;
  industry: string;
  address: string;
  linkToWebSite: string;
  logo: string;
}) {
  revalidatePath(routes.companies.path);
  return await createCompany(values.email, values.password, values.title, values.industry, values.address, values.linkToWebSite, values.logo);
}

export async function updateResumeAction(id: string, about: string) {
  revalidatePath(routes.profile.student.path);
  return await updateResume(id, about);
}

export async function getResumeByStudentIdAction(studentId: string) {
  return await getResumeByStudentId(studentId);
}
