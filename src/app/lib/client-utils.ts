import { useSession as useSessionNextAuth } from 'next-auth/react';
import { AppSession } from '@/app/lib/definitions';

export function useSession() {
  const { data } = useSessionNextAuth();
  return data as AppSession | null;
}
