import {
  Tag,
  Company,
  Direction,
  Group,
  Resume,
  Vacancy,
  Apply,
  User,
  IVacancyCreate,
  IDirectionCreate,
  IGroupCreate,
  IApplyCreate,
  Student,
} from './definitions';
import { pool } from './db';
import bcrypt from 'bcrypt';

function getCourse(year_education_start: number) {
  return Math.ceil((Date.now() - Date.parse(`09/01/${year_education_start}`)) / 1000 / 60 / 60 / 24 / 365);
}

export async function getCompanies() {
  try {
    const companies = await pool.query<Company>(
      `select c.id, c.user_id, c.title, c.industry, c.address, c.link_to_web_site, c.logo,
(select json_agg(row_to_json(v)) from vacancies v where c.id = v.company_id) as vacancies
from companies c`
    );

    return companies.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getCompanyById(id: string) {
  try {
    const company = await pool.query<Company>(
      `select c.id, c.user_id, c.title, c.industry, c.address, c.link_to_web_site, c.logo,
(select json_agg(row_to_json(v)) from vacancies v where v.company_id = c.id) as vacancies
from companies c
where c.id = '${id}'`
    );
    return company.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getCompanyByUserId(id: string) {
  try {
    const company = await pool.query<Company>(
      `select c.id, c.user_id, c.title, c.industry, c.address, c.link_to_web_site, c.logo,
(select json_agg(row_to_json(v)) from vacancies v where v.company_id = c.id) as vacancies
from companies c
where c.user_id = '${id}'`
    );
    return company.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getDirections() {
  try {
    const data = await pool.query<Direction>(`select * from directions`);

    return data.rows;
  } catch (error) {
    console.error('Database Error:', error);
    return [];
  }
}

export async function getDirectionById(id: string) {
  try {
    const data = await pool.query<Direction>(`select * from directions where directions.id = '${id}'`);

    return data.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch direction data.');
  }
}

export async function deleteDirection(id: string) {
  try {
    const query = `delete from directions WHERE id = $1;`;
    await pool.query(query, [id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to delete direction.');
  }
}

export async function getGroups() {
  try {
    const groups = await pool.query<Group>(
      `select g.id, g.direction_id, d.title as direction_title, g.years_of_study, g.education_start from groups g
left join directions d on g.direction_id = d.id`
    );

    return groups.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch group data.');
  }
}

export async function deleteGroup(id: string) {
  try {
    const query = `delete from groups WHERE id = $1;`;
    await pool.query(query, [id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to delete group.');
  }
}

export async function getGroupById(id: string) {
  try {
    const groups = await pool.query<Group>(
      `select g.id, g.direction_id, d.title as direction_title, g.years_of_study, g.education_start from groups g
left join directions d on g.direction_id = d.id where g.id = '${id}'`
    );

    return groups.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch group data.');
  }
}

export async function getTags() {
  try {
    const data = await pool.query<Tag>(`select * from tags`);

    return data.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch tags data.');
  }
}

export async function getTagById(id: string) {
  try {
    const tag = await pool.query<Tag>(`select * from tags where id = '${id}'`);

    return tag.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getTagByVacancyId(id: string) {
  try {
    const tags = await pool.query<Tag>(`
select * from tags t
left join vacancy_to_tag vtt on vtt.tag_id = t.id 
where vtt.vacancy_id = '${id}'`);

    return tags.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch tags data.');
  }
}

export async function getResumes() {
  try {
    const resume = await pool.query<Resume>(
      `select r.id, r.about, r.student_id, 
s.user_id, s.group_id, s.link_to_git, s.name, s.middle_name, s.lastname, s.phone, s.photo, s.birthday, s.telegram, s.average_score, s.status,
d.title as direction_title, g.title as group_title, g.education_start, 
(select json_agg(row_to_json(p)) from projects p where p.resume_id = r.id) as projects,
(select json_agg(row_to_json(t)) from tags t
left join student_to_tag stt on stt.tag_id = t.id
where stt.student_id = r.student_id ) as tags
from resumes r
left join students s on s.id = r.student_id 
left join groups g on g.id = s.group_id 
left join directions d on d.id = g.direction_id`
    );

    resume.rows.forEach((x) => (x.education_start = getCourse(x.education_start)));
    console.log(resume.rows);
    return resume.rows;
  } catch (error) {
    console.error('Database Error:', error);
    return [];
  }
}

export async function getResumeById(id: string) {
  try {
    const resume = await pool.query<Resume>(
      `select r.id, r.about, r.student_id, 
s.user_id, s.group_id, s.link_to_git, s.name, s.middle_name, s.lastname, s.phone, s.photo, s.birthday, s.telegram, s.average_score, s.status,
d.title as direction_title, g.title as group_title, g.education_start, 
(select json_agg(row_to_json(p)) from projects p where p.resume_id = r.id) as projects,
(select json_agg(row_to_json(t)) from tags t
left join student_to_tag stt on stt.tag_id = t.id
where stt.student_id = r.student_id ) as tags
from resumes r
left join students s on s.id = r.student_id 
left join groups g on g.id = s.group_id 
left join directions d on d.id = g.direction_id
where r.id = '${id}'`
    );

    resume.rows[0].education_start = getCourse(resume.rows[0].education_start);

    return resume.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getResumeByStudentId(id: string) {
  try {
    const resume = await pool.query<Resume>(
      `select r.id, r.about, r.student_id, 
s.user_id, s.group_id, s.link_to_git, s.name, s.middle_name, s.lastname, s.phone, s.photo, s.birthday, s.telegram, s.average_score, s.status,
d.title as direction_title, g.title as group_title, g.education_start, 
(select json_agg(row_to_json(p)) from projects p where p.resume_id = r.id) as projects,
(select json_agg(row_to_json(t)) from tags t
left join student_to_tag stt on stt.tag_id = t.id
where stt.student_id = r.student_id ) as tags
from resumes r
left join students s on s.id = r.student_id 
left join groups g on g.id = s.group_id 
left join directions d on d.id = g.direction_id
where r.student_id = '${id}'`
    );

    resume.rows[0].education_start = getCourse(resume.rows[0].education_start);

    return resume.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getVacancies() {
  try {
    const vacancy = await pool.query<Vacancy>(`
select v.id, v.company_id, v.title, v.description, v.type, v.date_from, v.date_to, v.expiration, v.task, v.city, v.is_remote,
(select json_agg(row_to_json(t)) from tags t
left join vacancy_to_tag vtt on vtt.tag_id = t.id
where vtt.vacancy_id = v.id ) as tags
from vacancies v`);

    console.log('===================', vacancy.rows);
    return vacancy.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getVacancyById(id: string) {
  try {
    const vacancy = await pool.query<Vacancy>(`
select v.id, v.company_id, v.title, v.description, v.type, v.date_from, v.date_to, v.expiration, v.task, v.city, v.is_remote,
(select json_agg(row_to_json(t)) from tags t
left join vacancy_to_tag vtt on vtt.tag_id = t.id
where vtt.vacancy_id = v.id ) as tags
from vacancies v
where v.id = '${id}'`);

    return vacancy.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getVacancyByCompanyId(id: string) {
  try {
    const vacancy = await pool.query<Vacancy>(`
select v.id, v.company_id, v.title, v.description, v.type, v.date_from, v.date_to, v.expiration, v.task, v.city, v.is_remote,
(select json_agg(row_to_json(t)) from tags t
left join vacancy_to_tag vtt on vtt.tag_id = t.id
where vtt.vacancy_id = v.id ) as tags
from vacancies v
where v.company_id = '${id}'`);

    return vacancy.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getApplyById(id: string) {
  try {
    const apply = await pool.query<Apply>(
      `select a.id, a.vacancy_id, v.title, a.student_id, a.task, a.is_applied, s.average_score, s.link_to_git,
(select count(p.id) from projects p
left join resumes r on p.resume_id = r.id
where r.student_id = a.student_id
) as count_project
from applies a 
left join vacancies v on v.id = a.vacancy_id 
left join students s on s.id = a.student_id
where a.id = '${id}'`
    );

    return apply.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getApplyByVacancyId(vacancyId: string) {
  try {
    const apply = await pool.query<Apply>(
      `select a.id, a.vacancy_id, v.title, a.student_id, a.task, a.is_applied, s.average_score, s.link_to_git,
(select count(p.id) from projects p
left join resumes r on p.resume_id = r.id
where r.student_id = a.student_id
) as count_project
from applies a 
left join vacancies v on v.id = a.vacancy_id 
left join students s on s.id = a.student_id
where a.vacancy_id = '${vacancyId}'`
    );

    return apply.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch companies data.');
  }
}

export async function getStudentById(id: string) {
  try {
    const resume = await pool.query<Resume>(
      `select r.id, r.about, r.student_id,
s.user_id, s.group_id, s.link_to_git, s."name", s.middle_name, s.lastname, s.phone, s.photo, s.birthday, s.telegram, s.average_score, s."status", s.is_approved,
d.title as direction_title, g.title as group_title, g.education_start,
(select json_agg(row_to_json(p)) from projects p where p.resume_id = r.id) as projects,
(select json_agg(row_to_json(t)) from tags t
left join student_to_tag stt on stt.tag_id = t.id
where stt.student_id = r.student_id ) as tags
from students s
left join groups g on g.id = s.group_id 
left join directions d on d.id = g.direction_id
left join resumes r on r.student_id = s.id
where id = '${id}'`
    );

    resume.rows[0].education_start = getCourse(resume.rows[0].education_start);

    return resume.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch resume data.');
  }
}

export async function getStudents() {
  try {
    const student = await pool.query<Student>(`
select * from students s`);

    return student.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch student data.');
  }
}

export async function getStudentByUserId(id: string) {
  try {
    const student = await pool.query<Student>(`
select s.id, s.user_id, s.group_id, r.id as resume_id, s.link_to_git, s.name, s.middle_name, s.lastname, s.phone, 
s.photo, s.birthday, s.telegram, s.average_score, s.status, s.is_approved
from students s 
left join resumes r on r.student_id = s.id
where user_id = '${id}'`);

    return student.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch student data.');
  }
}

export async function getStudentsNotApproved() {
  try {
    const student = await pool.query<Student>(`
select * from students s 
where s.is_approved = false`);

    return student.rows;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch student data.');
  }
}

export async function getUserById(id: string) {
  try {
    const resume = await pool.query<User>(`select * from Users where id = '${id}'`);

    return resume.rows[0];
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to fetch resume data.');
  }
}

export async function createVacancy(values: IVacancyCreate) {
  const client = await pool.connect();

  try {
    await client.query('BEGIN');

    const query = `
    insert into vacancies (company_id, title, description, type, date_from, date_to, expiration, task, city, is_remote) 
    values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [
      values.company_id,
      values.title,
      values.description,
      values.type,
      values.date_from,
      values.date_to,
      values.expiration,
      values.task,
      values.city,
      values.is_remote,
    ]);
    const queryCreateTag = `
    insert into vacancy_to_tag (tag_id, vacancy_id)
    values ($1, $2)`;

    values.tags?.forEach(async (x) => await pool.query(queryCreateTag, [x.id, id]));

    await client.query('COMMIT');

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  } finally {
    client.release();
  }
}

export async function updateVacancy(id: string, values: Vacancy) {
  const client = await pool.connect();

  try {
    await client.query('BEGIN');

    const queryDeleteTag = `delete from vacancy_to_tag WHERE vacancy_id = $1;`;
    await pool.query(queryDeleteTag, [values.id]);

    const queryCreateTag = `
    insert into vacancy_to_tag (tag_id, vacancy_id)
    values ($1, $2)`;

    values.tags?.forEach(async (x) => await pool.query(queryCreateTag, [x.id, id]));

    const query = `
    update vacancies
    set company_id = $1, title = $2, description = $3, type = $4, date_from = $5, date_to = $6, expiration = $7, task = $8, city = $9, is_remote = $10
    where id = $11`;
    await pool.query(query, [
      values.company_id,
      values.title,
      values.description,
      values.type,
      values.date_from,
      values.date_to,
      values.expiration,
      values.task,
      values.city,
      values.is_remote,
      id,
    ]);

    await client.query('COMMIT');
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  } finally {
    client.release();
  }
}

export async function createCompany(email: string, password: string, title: string, industry: string, address: string, linkToWebSite: string, logo: string) {
  const client = await pool.connect();

  try {
    const password_hash = await bcrypt.hash(password, 1);

    const queryText = `
    insert into users (email, password_hash, role) 
    values ($1, $2, 'company') returning id`;
    const {
      rows: [{ id }],
    } = await client.query<{ id: string }>(queryText, [email, password_hash]);

    const insertCompanyText = `
    insert into companies (user_id, title, industry, address, link_to_web_site, logo) 
    values ((select id from users where email = $1), $2, $3, $4, $5, $6) returning id as company_id`;
    const {
      rows: [{ company_id }],
    } = await pool.query<{ company_id: string }>(insertCompanyText, [email, title, industry, address, linkToWebSite, logo]);

    return company_id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create Company.');
  } finally {
    client.release();
  }
}

export async function createStudent(
  email: string,
  password: string,
  name: string,
  middleName: string,
  lastname: string,
  linkToGit: string,
  phone: string,
  birthday: any,
  telegram: string,
  status: string
) {
  const client = await pool.connect();

  try {
    const password_hash = await bcrypt.hash(password, 1);

    const queryText = `
    insert into users (email, password_hash, role) 
    values ($1, $2, 'student') returning id`;
    const {
      rows: [{ id }],
    } = await client.query<{ id: string }>(queryText, [email, password_hash]);

    const insertStudentText = `
    insert into students (user_id, name, middle_name, lastname, link_to_git, phone, birthday, telegram, status) 
    values ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning id as student_id`;
    const {
      rows: [{ student_id }],
    } = await pool.query<{ student_id: string }>(insertStudentText, [id, name, middleName, lastname, linkToGit, phone, birthday, telegram, status]);

    return student_id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create Student.');
  } finally {
    client.release();
  }
}

export async function updateStudent(values: Student) {
  try {
    const query = `
    update students
    set name = $1, middle_name = $2, lastname = $3, link_to_git = $4, phone = $5, birthday = $6, telegram = $7, status = $8
    where id = $9`;
    await pool.query(query, [
      values.name,
      values.middle_name,
      values.lastname,
      values.link_to_git,
      values.phone,
      values.birthday,
      values.telegram,
      values.status,
      values.id,
    ]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to update Student.');
  }
}

export async function updateStudentGroup(id: string, group_id: string) {
  try {
    const query = `
    update students
    set group_id = $1
    where id = $2`;
    await pool.query(query, [group_id, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to updateStudentGroup.');
  }
}

export async function updateStudentAverageScore(id: string, average_score: number) {
  try {
    const query = `
    update students
    set average_score = $1
    where id = $2`;
    await pool.query(query, [average_score, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to updateStudentAverageScore.');
  }
}

export async function updateStudentApproved(id: string, isApproved: boolean) {
  try {
    const query = `
    update students
    set is_approved = $1
    where id = $2`;
    await pool.query(query, [isApproved, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to updateStudentApproved.');
  }
}

export async function updateCompany(id: string, title: string, industry: string, address: string, linkToWebSite: string, logo: string) {
  try {
    const query = `
    update companies
    set title = $1, industry = $2, address = $3, link_to_web_site = $4, logo = $5
    where id = $6`;
    await pool.query(query, [title, industry, address, linkToWebSite, logo, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function createGroup(values: IGroupCreate) {
  try {
    const query = `
    insert into groups (title, direction_id, years_of_study, education_start) 
    values ($1, $2, $3, $4) returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [values.direction_title, values.direction_id, values.years_of_study, values.education_start]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function updateGroup(id: string, values: Group) {
  try {
    const query = `
    update groups
    set title = $1, direction_id = $2, years_of_study = $3, education_start = $4
    where id = $5`;
    await pool.query(query, [values.direction_title, values.direction_id, values.years_of_study, values.education_start, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function createDirection(values: IDirectionCreate) {
  try {
    const query = `
    insert into directions (title, description) 
    values ($1, $2) returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [values.title, values.description]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function updateDirection(id: string, values: IDirectionCreate) {
  try {
    const query = `
    update directions
    set title = $1, description = $2
    where id = $3`;
    await pool.query(query, [values.title, values.description, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function createTag(title: string) {
  try {
    const query = `
    insert into tags (title) 
    values ($1) returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [title]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function updateTag(id: string, title: string) {
  try {
    const query = `
    update tags
    set title = $1
    where id = $2`;
    await pool.query(query, [title, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create vacancy.');
  }
}

export async function deleteTag(id: string) {
  try {
    const query = `delete from tags WHERE id = $1;`;
    await pool.query(query, [id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to delete tag.');
  }
}

export async function updateStudentToTag(id: string, values: Tag[]) {
  const client = await pool.connect();

  try {
    await client.query('BEGIN');

    const queryDeleteTag = `delete from student_to_tag WHERE student_id = $1;`;
    await pool.query(queryDeleteTag, [id]);

    const queryCreateTag = `
    insert into student_to_tag (tag_id, student_id)
    values ($1, $2)`;

    values?.forEach(async (x) => await pool.query(queryCreateTag, [x.id, id]));

    await client.query('COMMIT');
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to updateStudentToTag.');
  } finally {
    client.release();
  }
}

export async function createProject(resumeId: string, dateFrom: any, dateTo: any, title: string, description: string) {
  try {
    const query = `
    insert into projects (resume_id, date_from, date_to, title, description) values ($1, $2, $3, $4, $5) 
    returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [resumeId, dateFrom, dateTo, title, description]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create project.');
  }
}

export async function updateProject(id: string, dateFrom: any, dateTo: any, title: string, description: string) {
  try {
    const query = `
    update projects
    set date_from = $1, date_to = $2, title = $3, description = $4
    where id = $5`;
    await pool.query(query, [dateFrom, dateTo, title, description, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create project.');
  }
}

export async function deleteProject(id: string) {
  try {
    const query = `delete from projects WHERE id = $1;`;
    await pool.query(query, [id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to delete project.');
  }
}

export async function createApply(values: IApplyCreate) {
  try {
    const query = `
    insert into applies (vacancy_id, student_id, task) values ($1, $2, $3) 
    returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [values.vacancy_id, values.student_id, values.task]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create apply.');
  }
}

export async function updateApply(id: string, isApplied?: boolean) {
  try {
    const query = `
    update applies
    set is_applied = $1
    where id = $2`;
    await pool.query(query, [isApplied, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to update apply.');
  }
}

export async function createResume(studentId: string, about: string) {
  try {
    const query = `
    insert into resumes (about, student_id)
    values ($1, $2) returning id`;

    const {
      rows: [{ id }],
    } = await pool.query<{ id: string }>(query, [about, studentId]);

    return id;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to create resume.');
  }
}

export async function updateResume(id: string, about: string) {
  try {
    const query = `
    update resumes
    set about = $1
    where id = $2`;
    await pool.query(query, [about, id]);
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to update resume.');
  }
}

export async function getUserByEmail(email: string) {
  try {
    const query = `select * from users where email = $1`;
    const {
      rows: [user],
    } = await pool.query<User>(query, [email]);
    return user;
  } catch (error) {
    console.error('Database Error:', error);
    throw new Error('Failed to get user by email.');
  }
}
