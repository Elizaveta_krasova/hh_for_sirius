import { Session, User as NextAuthUser } from 'next-auth';

export type Tag = {
  id: string;
  title: string;
};

export type ITagColor = 'green' | 'red' | 'yellow' | 'blue' | 'orange' | 'white' | 'gray';

export type Group = {
  id: string;
  direction_id: string;
  direction_title: string;
  years_of_study: number;
  education_start: number;
};

export interface IGroupCreate {
  id?: string;
  direction_id?: string;
  direction_title?: string;
  years_of_study?: number;
  education_start?: number;
}

export type Direction = {
  id: string;
  title: string;
  description: string;
};

export interface IDirectionCreate {
  id?: string;
  title?: string;
  description?: string;
}

export type User = {
  id: string;
  email: string;
  role: EUserRole;
  password_hash: string;
};

export enum EUserRole {
  STUDENT = 'student',
  COMPANY = 'company',
  ADMIN = 'admin',
}

export type Resume = {
  id: string;
  about: string;
  student_id: string;
  projects: Project[];
  user_id: string;
  group_id: string;
  link_to_git: string;
  name: string;
  middle_name: string;
  lastname: string;
  phone: string;
  photo: string;
  birthday: Date;
  telegram: string;
  average_score: any;
  status: string;
  direction_title: string;
  group_title: string;
  education_start: number;
  tags: Tag[];
  is_approved: string;
};

export type Apply = {
  id: string;
  vacancy_id: string;
  title: string; //title vacancy
  student_id: string;
  task: string; //response
  is_applied: any; // true | false | null
  average_score: any;
  link_to_git: string;
  count_project: number;
};

export interface IApplyCreate {
  vacancy_id: string;
  student_id: string;
  task?: string;
}

export type Student = {
  id: string;
  user_id: string;
  group_id: string;
  resume_id: string;
  link_to_git: string;
  name: string;
  middle_name: string;
  lastname: string;
  phone: string;
  photo: string;
  birthday: Date;
  telegram: string;
  average_score: any;
  status: IStudentStatus;
  is_approved: string;
};

export enum IStudentStatus {
  PRACTICE = 'practice',
  INTERN = 'internship',
  job = 'job',
  NOT_LOOKING_FOR_JOB = 'not_looking_FOR_JOB',
}

export type Project = {
  id: string;
  resume_id: string;
  date_from: Date;
  date_to: Date;
  title: string;
  description: string;
};

export type IProjectCreate = {
  date_from?: Date;
  date_to?: Date;
  title?: string;
  description?: string;
};

export type Vacancy = {
  id: string;
  company_id: string;
  title: string;
  description: string;
  type: EVacancyType;
  date_from: Date;
  date_to: Date;
  expiration: Date;
  task?: string;
  city: string;
  is_remote: boolean;
  tags?: Tag[];
};

export interface IVacancyCreate {
  id?: string;
  company_id?: string;
  title?: string;
  description?: string;
  type?: EVacancyType;
  date_from?: Date;
  date_to?: Date;
  expiration?: Date;
  task?: string;
  city?: string;
  is_remote?: boolean;
  tags?: Tag[];
}

export enum EVacancyType {
  PRACTICE = 'practice',
  INTERNSHIP = 'internship',
  JOB = 'job',
}

export type Company = {
  id: string;
  user_id: string;
  title: string;
  industry: string;
  address: string;
  link_to_web_site?: string;
  logo?: string;
  vacancies?: Vacancy[];
};

export type AppSession = Session;

declare module 'next-auth' {
  interface Session {
    user: {
      role: EUserRole;
      student?: Student;
      company?: Company;
    } & NextAuthUser;
  }
}
