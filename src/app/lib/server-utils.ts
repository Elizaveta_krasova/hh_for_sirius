import { getServerSession as getServerSessionNextAuth } from 'next-auth/next';
import { AuthOptions } from '@/app/api/auth/[...nextauth]/route';
import { AppSession } from '@/app/lib/definitions';

export async function getServerSession(): Promise<AppSession | null> {
  return await getServerSessionNextAuth<{}, AppSession>(AuthOptions);
}
