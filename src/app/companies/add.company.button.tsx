'use client';
import React, { FC, useState } from 'react';
import Button from '@/app/components/button';
import AddCompanyModal from '@/app/companies/add.company.modal';

const AddCompanyButton: FC = () => {
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <Button text="Добавить компанию" color="green" onClick={() => setIsShowModal(true)} />
      <AddCompanyModal open={isShowModal} close={() => setIsShowModal(false)} />
    </>
  );
};

export default AddCompanyButton;
