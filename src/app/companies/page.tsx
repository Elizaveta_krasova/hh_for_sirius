import React from 'react';

import Card from '@/app/companies/card';
import { getCompanies } from '@/app/lib/data';
import AddCompanyButton from '@/app/companies/add.company.button';
import { getServerSession } from '@/app/lib/server-utils';
import { EUserRole } from '@/app/lib/definitions';

export default async function Page() {
  const companies = await getCompanies();
  const session = await getServerSession();

  return (
    <>
      {session?.user?.role === EUserRole.ADMIN && (
        <div className="p-6">
          <AddCompanyButton />
        </div>
      )}
      <div role="list" className="p-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {companies.map((company) => (
          <Card company={company} key={company.id} className="col-span-1" />
        ))}
      </div>
    </>
  );
}
