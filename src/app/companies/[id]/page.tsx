import React from 'react';
import VacancyCard from '@/app/vacancies/card';
import { getCompanyById } from '@/app/lib/data';
import BackButton from '@/app/components/back.button';

interface IPageProps {
  params: { id: string };
}

export default async function Page({ params }: IPageProps) {
  const company = await getCompanyById(params.id);

  return (
    <div className="h-full overflow-y-auto">
      <div className="mx-auto max-w-[900px] mt-6">
        <BackButton />
      </div>
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <div className="flex flex-col gap-4">
          <div className="flex flex-col md:flex-row gap-4 justify-between">
            <div className="px-4 sm:px-0">
              <h3 className="text-lg font-semibold leading-7 text-gray-900">Информация</h3>
              <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Личные данные</p>
            </div>
          </div>
          <div className="border-t border-gray-100">
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Название</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.title}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Направление</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.industry}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Адрес</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.address}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Ссылка</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.link_to_web_site}</dd>
              </div>
            </dl>
          </div>

          {!!company.vacancies?.length && (
            <div className="flex flex-col gap-6 w-full">
              <span className="text-xl font-semibold">Вакансии</span>

              <div role="list" className="w-full flex gap-4 overflow-y-auto">
                {company.vacancies?.map((vacancy) => (
                  <VacancyCard id={vacancy.id} key={vacancy.id} hideApply />
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
