'use client';
import React, { FC } from 'react';
import { ChevronRightIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import { Company } from '@/app/lib/definitions';
import { useSession } from '../lib/client-utils';
import cx from 'classnames';
import { PropsWithClassName } from '@/app/types/common';

interface ICardProps {
  company: Company;
}

const Card: FC<PropsWithClassName<ICardProps>> = ({ company, className }) => {
  const session = useSession();
  console.log(session?.user.role);

  return (
    <Link
      href={routes.companies.company.generatePath(company.id)}
      type="button"
      className={cx(
        'bg-white w-full min-w-[300px] relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center',
        className
      )}
    >
      <div className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start">
        <p className="font-bold text-xl text-gray-900">{company.title}</p>
        <p className="font-semibold text-lg text-gray-700 leading-6">{company.industry}</p>
        <p>Address: {company.address}</p>
        {!!company.vacancies?.length && <p>Вакансии: {company.vacancies?.length}</p>}
        {!company.vacancies?.length && <p>Нет вакансий</p>}
      </div>
    </Link>
  );
};

export default Card;
