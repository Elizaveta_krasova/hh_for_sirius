'use client';
import React, { FC } from 'react';
import { Dialog, DialogPanel, DialogTitle, Transition, TransitionChild } from '@headlessui/react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { useFormik } from 'formik';
import { createCompanyAction } from '@/app/lib/actions';
import { routes } from '@/app/config/routes';
import { useRouter } from 'next/navigation';

interface IAddCompanyModalProps {
  open: boolean;
  close: () => void;
}

const AddCompanyModal: FC<IAddCompanyModalProps> = ({ open: receivedOpen, close }) => {
  const router = useRouter();
  const formik = useFormik<{
    email: string;
    password: string;
    title: string;
    industry: string;
    address: string;
    linkToWebSite: string;
    logo: string;
  }>({
    initialValues: {
      email: '',
      password: '',
      title: '',
      industry: '',
      address: '',
      linkToWebSite: '',
      logo: '',
    },
    validate: (values: { email: string; password: string; title: string; industry: string; address: string; linkToWebSite: string; logo: string }) => {
      const errors: any = {};
      if (!values.email) errors.email = true;
      if (!values.password) errors.password = true;
      if (!values.title) errors.title = true;
      if (!values.industry) errors.industry = true;
      if (!values.address) errors.address = true;
      if (!values.linkToWebSite) errors.linkToWebSite = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await createCompanyAction({ ...values });
        router.push(routes.companies.path);
        close();
      } catch (e) {}
    },
  });

  return (
    <Transition show={receivedOpen}>
      <Dialog className="relative z-10" onClose={close}>
        <TransitionChild
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center">
            <TransitionChild
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <DialogPanel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full max-w-[700px] sm:p-6">
                <div className="flex flex-col gap-4">
                  <DialogTitle className="font-semibold">Создание компании</DialogTitle>
                  <div className="max-h-[500px] overflow-y-auto">
                    <form onSubmit={formik.handleSubmit}>
                      <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">
                          <div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.title}
                                name="title"
                                value={formik.values.title}
                                onChange={(value) => formik.setFieldValue('title', value)}
                                label="Название"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.industry}
                                name="industry"
                                value={formik.values.industry}
                                onChange={(value) => formik.setFieldValue('industry', value)}
                                label="Направление"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.password}
                                name="password"
                                value={formik.values.password}
                                onChange={(value) => formik.setFieldValue('password', value)}
                                label="Пароль"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.linkToWebSite}
                                name="linkToWebSite"
                                value={formik.values.linkToWebSite}
                                onChange={(value) => formik.setFieldValue('linkToWebSite', value)}
                                label="Ссылка"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.address}
                                name="address"
                                value={formik.values.address}
                                onChange={(value) => formik.setFieldValue('address', value)}
                                label="Адрес"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.email}
                                name="email"
                                value={formik.values.email}
                                onChange={(value) => formik.setFieldValue('email', value)}
                                label="Почта"
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="mt-6 flex items-center justify-end gap-x-6">
                        <Button text="Отмена" color="white" onClick={() => close()} />

                        <Button type="submit" text="Сохранить" color="green" />
                      </div>
                    </form>
                  </div>
                </div>
              </DialogPanel>
            </TransitionChild>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default AddCompanyModal;
