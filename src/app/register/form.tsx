'use client';
import React from 'react';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import TextField from '@/app/components/text.field';
import { useFormik } from 'formik';
import { createResumeAction, createStudentAction } from '@/app/lib/actions';
import { useRouter } from 'next/navigation';
import { IStudentStatus } from '@/app/lib/definitions';
import DateField from '@/app/components/date.field';
import SelectBox from '@/app/components/select.box';
import Button from '@/app/components/button';

const Form = () => {
  const router = useRouter();
  const formik = useFormik<{
    email: string;
    password: string;
    name: string;
    middleName: string;
    lastname: string;
    linkToGit: string;
    phone: string;
    birthday: any;
    telegram: string;
    status: string;
  }>({
    initialValues: {
      email: '',
      password: '',
      lastname: '',
      middleName: '',
      name: '',
      birthday: new Date(),
      linkToGit: '',
      phone: '',
      telegram: '',
      status: IStudentStatus.PRACTICE,
    },
    validate: (values: {
      email: string;
      password: string;
      name: string;
      middleName: string;
      lastname: string;
      linkToGit: string;
      phone: string;
      birthday: any;
      telegram: string;
      status: string;
    }) => {
      const errors: any = {};
      if (!values.email) errors.email = true;
      if (!values.password) errors.password = true;
      if (!values.middleName) errors.middleName = true;
      if (!values.name) errors.name = true;
      if (!values.birthday) errors.birthday = true;
      if (!values.phone) errors.phone = true;
      if (!values.status) errors.status = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        const studentId = await createStudentAction({ ...values });

        await createResumeAction(studentId, '');

        router.push(routes.login.path);
      } catch (e) {}
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} className="flex w-full h-full items-center justify-center">
      <div className="flex flex-col justify-center p-6 rounded-lg m-auto max-h-min bg-white">
        <div className="sm:mx-auto">
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">Регистрация</h2>
        </div>

        <div className="mt-10 sm:mx-auto">
          <div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-9">
            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.email}
                name="email"
                value={formik.values.email}
                onChange={(value) => formik.setFieldValue('email', value)}
                label="Почта"
              />
            </div>

            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.password}
                name="password"
                value={formik.values.password}
                onChange={(value) => formik.setFieldValue('password', value)}
                label="Пароль"
              />
            </div>
            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.name}
                name="name"
                value={formik.values.name}
                onChange={(value) => formik.setFieldValue('name', value)}
                label="Имя"
              />
            </div>
            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.lastname}
                name="lastname"
                value={formik.values.lastname}
                onChange={(value) => formik.setFieldValue('lastname', value)}
                label="Фамилия"
              />
            </div>
            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.middleName}
                name="middleName"
                value={formik.values.middleName}
                onChange={(value) => formik.setFieldValue('middleName', value)}
                label="Отчество"
              />
            </div>
            <div className="sm:col-span-3">
              <DateField
                error={!!formik.errors.birthday}
                name="birthday"
                value={formik.values.birthday}
                onChange={(value) => formik.setFieldValue('birthday', value)}
                label="День Рождения"
              />
            </div>
            <div className="sm:col-span-3">
              <SelectBox
                items={[
                  { value: IStudentStatus.job, text: 'Работа' },
                  { value: IStudentStatus.INTERN, text: 'Интернaтура' },
                  { value: IStudentStatus.PRACTICE, text: 'Практика' },
                  { value: IStudentStatus.NOT_LOOKING_FOR_JOB, text: 'Не ищу работу' },
                ]}
                error={!!formik.errors.status}
                value={formik.values.status}
                onChange={(value) => formik.setFieldValue('status', value)}
                label="Статус"
              />
            </div>
            <div className="sm:col-span-3">
              <TextField
                error={!!formik.errors.phone}
                name="phone"
                value={formik.values.phone}
                onChange={(value) => formik.setFieldValue('phone', value)}
                label="Телефон"
              />
            </div>
            <div className="sm:col-span-6">
              <TextField
                error={!!formik.errors.linkToGit}
                name="linkToGit"
                value={formik.values.linkToGit}
                onChange={(value) => formik.setFieldValue('linkToGit', value)}
                label="Ссылка Git"
              />
            </div>
            <div className="sm:col-span-3">
              <TextField name="telegram" value={formik.values.telegram} onChange={(value) => formik.setFieldValue('telegram', value)} label="Телеграмм" />
            </div>
            <div className="sm:col-span-9">
              <Button type="submit" text="Зарегистрироваться" color="green" />
            </div>
          </div>

          <p className="mt-6 text-center text-sm text-gray-500">
            <Link href={routes.login.path} className="font-semibold leading-6 text-blue-600 hover:text-blue-500">
              Авторизация
            </Link>
          </p>
        </div>
      </div>
    </form>
  );
};

export default Form;
