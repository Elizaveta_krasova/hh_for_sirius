import React from 'react';

import Card from '@/app/groups/card';
import { getGroups } from '@/app/lib/data';
import AddGroupButton from '@/app/groups/add.group.button';

export default async function Page() {
  const groups = await getGroups();
  return (
    <div className="flex flex-col gap-4">
      <div className="flex justify-end p-6 pb-0">
        <div>
          <AddGroupButton />
        </div>
      </div>
      <div role="list" className="p-6 flex flex-col gap-4">
        {groups.map((group) => (
          <Card group={group} key={group.id} />
        ))}
      </div>
    </div>
  );
}
