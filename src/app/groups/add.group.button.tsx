'use client';
import React, { FC } from 'react';
import Button from '@/app/components/button';
import Link from 'next/link';
import { routes } from '@/app/config/routes';

const AddGroupButton: FC = () => {
  return (
    <>
      <Link href={routes.groups.create.path}>
        <Button text="Добавить группу" color="green" />
      </Link>
    </>
  );
};

export default AddGroupButton;
