import { getDirections, getGroupById } from '@/app/lib/data';
import { Form } from '@/app/groups/[id]/edit/form';

export default async function Page({ params }: { params: { id: string } }) {
  const group = await getGroupById(params.id);
  const directions = await getDirections();

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form directions={directions} group={group} />
      </div>
    </div>
  );
}
