'use client';
import React from 'react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { useRouter } from 'next/navigation';
import { routes } from '@/app/config/routes';
import { useFormik } from 'formik';
import { createGroupAction } from '@/app/lib/actions';
import { Direction, IGroupCreate } from '@/app/lib/definitions';
import SelectBox from '@/app/components/select.box';

export function Form({ directions }: { directions: Direction[] }) {
  const router = useRouter();

  const formik = useFormik<IGroupCreate>({
    initialValues: {
      direction_id: '',
      years_of_study: undefined,
      education_start: undefined,
    },
    validate: (values: IGroupCreate) => {
      const errors: any = {};
      if (!values.direction_id) errors.direction_id = true;
      if (!values.years_of_study) errors.years_of_study = true;
      if (!values.education_start) errors.education_start = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        const directionName = directions.find((d) => d.id === values.direction_id)?.title;
        await createGroupAction({ ...values, direction_title: directionName });
        router.push(routes.groups.path);
      } catch (e) {}
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="space-y-12">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-base font-semibold leading-7 text-gray-900">Группа</h2>
          <p className="mt-1 text-sm leading-6 text-gray-600">Создание группы</p>

          <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-10">
            <div className="sm:col-span-6">
              <SelectBox
                error={!!formik.errors.direction_id}
                value={formik.values.direction_id}
                items={directions.map((direction) => ({ value: direction.id, text: direction.title }))}
                onChange={(value) => formik.setFieldValue('direction_id', value)}
                label="Выбор направления"
              />
            </div>

            <div className="sm:col-span-6">
              <TextField
                error={!!formik.errors.years_of_study}
                name="years_of_study"
                value={formik.values.years_of_study}
                onChange={(value) => formik.setFieldValue('years_of_study', value)}
                label="years_of_study"
              />
            </div>

            <div className="sm:col-span-6">
              <TextField
                error={!!formik.errors.education_start}
                name="education_start"
                value={formik.values.education_start}
                onChange={(value) => formik.setFieldValue('education_start', value)}
                label="education_start"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-6 flex items-center justify-end gap-x-6">
        <Button text="Отмена" color="white" onClick={() => router.push(routes.groups.path)} />

        <Button type="submit" text="Сохранить" color="green" />
      </div>
    </form>
  );
}
