import { Form } from '@/app/groups/create/form';
import { getDirections } from '@/app/lib/data';

export default async function Page() {
  const directions = await getDirections();

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form directions={directions} />
      </div>
    </div>
  );
}
