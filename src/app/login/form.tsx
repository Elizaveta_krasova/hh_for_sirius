'use client';
import React from 'react';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import { signIn } from 'next-auth/react';
import { useFormik } from 'formik';
import TextField from '@/app/components/text.field';

const Form = () => {
  const formik = useFormik<{
    email: string;
    password: string;
  }>({
    initialValues: {
      email: '',
      password: '',
    },
    validate: (values: { email: string; password: string }) => {
      const errors: any = {};
      if (!values.email) errors.email = true;
      if (!values.password) errors.password = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        const result = await signIn('credentials', {
          email: values.email,
          password: values.password,
          redirect: true,
          callbackUrl: routes.vacancies.path,
        });
      } catch (e) {}
    },
  });
  return (
    <form onSubmit={formik.handleSubmit} className="flex w-full h-full items-center justify-center">
      <div className="flex flex-col justify-center p-6 rounded-lg m-auto w-full h-full max-w-[500px] max-h-[500px] bg-white">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">Авторизация</h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <div className="space-y-6">
            <div>
              <TextField
                error={!!formik.errors.email}
                name="email"
                value={formik.values.email}
                onChange={(value) => formik.setFieldValue('email', value)}
                label="Почта"
              />
            </div>

            <div>
              <TextField
                error={!!formik.errors.password}
                name="password"
                value={formik.values.password}
                onChange={(value) => formik.setFieldValue('password', value)}
                label="Пароль"
              />
            </div>

            <div>
              <button
                // onClick={() => signIn('Credentials', { redirect: true }, { email: 'bill@fillmurray.com', username: 'username' })}
                type="submit"
                className="flex w-full justify-center rounded-md bg-blue-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-blue-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-blue-600"
              >
                Войти
              </button>
            </div>
          </div>

          <p className="mt-6 text-center text-sm text-gray-500">
            <Link href={routes.register.path} className="font-semibold leading-6 text-blue-600 hover:text-blue-500">
              Регистрация
            </Link>
          </p>
        </div>
      </div>
    </form>
  );
};

export default Form;
