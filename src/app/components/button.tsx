'use client';
import React, { FC, SyntheticEvent } from 'react';
import cx from 'classnames';

type IButtonColor = 'green' | 'red' | 'blue' | 'gray' | 'white';

interface IButtonProps {
  text: string;
  onClick?: (e: SyntheticEvent) => void;
  color?: IButtonColor;
  type?: 'button' | 'submit';
  disabled?: boolean;
}

const getColor = (color: IButtonColor) => {
  switch (color) {
    case 'red':
      return 'bg-red-600 hover:bg-red-500 focus-visible:outline-red-600';
    case 'green':
      return 'bg-green-600 hover:bg-green-500 focus-visible:outline-green-600';
    case 'gray':
      return 'bg-gray-400 hover:bg-gray-300 focus-visible:outline-gray-400';
    case 'white':
      return 'border !text-gray-600 border-gray-400 hover:border-gray-300 focus-visible:outline-gray-400 bg-white';
    case 'blue':
    default:
      return 'bg-blue-600 hover:bg-blue-500 focus-visible:outline-blue-600';
  }
};

const Button: FC<IButtonProps> = ({ disabled, type = 'button', color = 'blue', onClick, text }) => {
  return (
    <button
      onClick={onClick}
      type={type}
      disabled={disabled}
      className={cx(
        { [getColor(color)]: !disabled },
        { 'bg-gray-700': disabled },
        'inline-flex w-min whitespace-nowrap justify-center rounded-md px-3 py-2 text-sm font-medium text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2'
      )}
    >
      {text}
    </button>
  );
};
export default Button;
