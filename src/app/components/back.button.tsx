'use client';
import React, { FC } from 'react';
import Button from '@/app/components/button';
import { useRouter } from 'next/navigation';

const BackButton: FC = () => {
  const router = useRouter();

  return (
    <div className="flex justify-start">
      <Button text="Назад" color="gray" onClick={router.back} />
    </div>
  );
};
export default BackButton;
