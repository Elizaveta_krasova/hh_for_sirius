'use client';
import cx from 'classnames';
import { FC, Fragment, useMemo, useState } from 'react';
import { Label, Listbox, ListboxButton, ListboxOption, ListboxOptions, Transition } from '@headlessui/react';
import { CheckIcon, ChevronDownIcon } from '@heroicons/react/solid';
import { PropsWithClassName } from '@/app/types/common';

interface ISelectBoxProps {
  value?: string | number | boolean;
  onChange: (value: number | string | boolean) => void;
  items: { text: string; value: number | string | boolean }[];
  label: string;
  error?: boolean;
}

const SelectBox: FC<PropsWithClassName<ISelectBoxProps>> = ({ className, error, value, onChange, items, label }) => {
  const name = useMemo(() => {
    return items.find((item) => item.value === value)?.text ?? '';
  }, [items, value]);

  return (
    <Listbox value={value} onChange={(value) => onChange(value)}>
      {({ open }) => (
        <div className={className}>
          <Label className={cx('block text-sm font-medium leading-6 text-gray-900', { '!text-red-500': error })}>{label}</Label>
          <div className="relative mt-2">
            <ListboxButton className="h-[36px] relative w-full cursor-default rounded-md bg-white py-1.5 pl-3 pr-10 text-left text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:outline-none focus:ring-2 focus:ring-blue-600 sm:text-sm sm:leading-6">
              <span className="block truncate">{name}</span>
              <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                <ChevronDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
              </span>
            </ListboxButton>

            <Transition show={open} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
              <ListboxOptions className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                {items.map((item) => (
                  <ListboxOption
                    key={String(item.value)}
                    className={({ focus }) =>
                      cx(focus ? 'bg-blue-600 text-white' : '', !focus ? 'text-gray-900' : '', 'relative cursor-default select-none py-2 pl-3 pr-9')
                    }
                    value={item.value}
                  >
                    {({ selected, focus }) => (
                      <>
                        <span className={cx(selected ? 'font-semibold' : 'font-normal', 'block truncate')}>{item.text}</span>

                        {selected ? (
                          <span className={cx(focus ? 'text-white' : 'text-blue-600', 'absolute inset-y-0 right-0 flex items-center pr-4')}>
                            <CheckIcon className="h-5 w-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </ListboxOption>
                ))}
              </ListboxOptions>
            </Transition>
          </div>
        </div>
      )}
    </Listbox>
  );
};

export default SelectBox;
