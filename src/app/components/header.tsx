'use client';
import React, { useState } from 'react';
import { Button, Dialog, DialogPanel, PopoverGroup } from '@headlessui/react';
import { MenuIcon, XIcon } from '@heroicons/react/outline';
import { routes } from '@/app/config/routes';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import cx from 'classnames';
import { signOut } from 'next-auth/react';
import useUser from '@/app/hooks/use.user';

const isActive = (pathname: string, includePath: string) => pathname.includes(includePath);

const Header = ({ isAuth }: { isAuth: boolean }) => {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const pathname = usePathname();
  const { isAdmin, isCompany, isStudent } = useUser();
  return (
    <header className="bg-white">
      <nav className="mx-auto flex items-center justify-between p-6" aria-label="Global">
        <div className="flex lg:flex-1">
          <Link href={routes.root.path} className="text-lg font-semibold leading-6 text-gray-900">
            HH Sirius
          </Link>
        </div>
        <div className="flex lg:hidden">
          <button
            type="button"
            className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
            onClick={() => setMobileMenuOpen(true)}
          >
            <span className="sr-only">Open main menu</span>
            <MenuIcon className="h-6 w-6" aria-hidden="true" />
          </button>
        </div>
        {isAuth && (
          <PopoverGroup className="hidden lg:flex lg:gap-x-12">
            <Link
              href={routes.vacancies.path}
              className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.vacancies.path) })}
            >
              Вакансии
            </Link>
            {!isStudent && (
              <Link
                href={routes.resumes.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.resumes.path) })}
              >
                Резюме
              </Link>
            )}
            {!isCompany && (
              <Link
                href={routes.companies.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.companies.path) })}
              >
                Компании
              </Link>
            )}
            {isAdmin && (
              <Link
                href={routes.directions.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.directions.path) })}
              >
                Направления
              </Link>
            )}
            {isAdmin && (
              <Link
                href={routes.groups.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.groups.path) })}
              >
                Группы
              </Link>
            )}
            {isAdmin && (
              <Link
                href={routes.tags.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.tags.path) })}
              >
                Теги
              </Link>
            )}
            {isAdmin && (
              <Link
                href={routes.applies.students.path}
                className={cx('text-sm font-semibold leading-6 text-gray-900', { '!text-blue-500': isActive(pathname, routes.applies.students.path) })}
              >
                Студенты
              </Link>
            )}
          </PopoverGroup>
        )}
        <div className="hidden lg:flex lg:flex-1 lg:justify-end text-sm font-semibold leading-6 text-gray-900">
          {isAuth ? (
            <div className="flex gap-8 items-center">
              {isStudent && (
                <Link
                  href={routes.profile.student.path}
                  className={cx('text-sm font-semibold leading-6', {
                    '!text-blue-500': isActive(pathname, routes.profile.student.path),
                  })}
                >
                  Профиль
                </Link>
              )}
              {isCompany && (
                <Link
                  href={routes.profile.company.path}
                  className={cx('text-sm font-semibold leading-6', {
                    '!text-blue-500': isActive(pathname, routes.profile.company.path),
                  })}
                >
                  Профиль
                </Link>
              )}

              <Button onClick={() => signOut()}>Выйти</Button>
            </div>
          ) : (
            <Link href={routes.login.path} className="">
              Войти
            </Link>
          )}
        </div>
      </nav>
      <Dialog className="lg:hidden" open={mobileMenuOpen} onClose={setMobileMenuOpen}>
        <div className="fixed inset-0 z-10" />
        <DialogPanel className="fixed inset-y-0 right-0 z-10 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
          <div className="flex items-center justify-between">
            <button type="button" className="-m-2.5 rounded-md p-2.5 text-gray-700" onClick={() => setMobileMenuOpen(false)}>
              <span className="sr-only">Close menu</span>
              <XIcon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <div className="mt-6 flow-root">
            <div className="-my-6 divide-y divide-gray-500/10">
              <div className="space-y-2 py-6">
                {isAuth && (
                  <>
                    <Link
                      href={routes.vacancies.path}
                      className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                        '!text-blue-500': isActive(pathname, routes.vacancies.path),
                      })}
                    >
                      Вакансии
                    </Link>
                    {!isStudent && (
                      <Link
                        href={routes.resumes.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.resumes.path),
                        })}
                      >
                        Резюме
                      </Link>
                    )}
                    {!isCompany && (
                      <Link
                        href={routes.companies.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.companies.path),
                        })}
                      >
                        Компании
                      </Link>
                    )}
                    {isAdmin && (
                      <Link
                        href={routes.directions.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.directions.path),
                        })}
                      >
                        Направления
                      </Link>
                    )}
                    {isAdmin && (
                      <Link
                        href={routes.groups.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.groups.path),
                        })}
                      >
                        Группы
                      </Link>
                    )}
                    {isAdmin && (
                      <Link
                        href={routes.tags.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.tags.path),
                        })}
                      >
                        Теги
                      </Link>
                    )}
                    {isAdmin && (
                      <Link
                        href={routes.applies.students.path}
                        className={cx('-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50', {
                          '!text-blue-500': isActive(pathname, routes.applies.students.path),
                        })}
                      >
                        Студенты
                      </Link>
                    )}
                  </>
                )}
                {isAuth ? (
                  <>
                    <>
                      {isStudent && (
                        <Link
                          href={routes.profile.student.path}
                          className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                        >
                          Профиль
                        </Link>
                      )}
                      {isCompany && (
                        <Link
                          href={routes.profile.company.path}
                          className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                        >
                          Профиль
                        </Link>
                      )}
                    </>
                    <Button
                      className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                      onClick={() => signOut()}
                    >
                      Выйти
                    </Button>
                  </>
                ) : (
                  <Link href={routes.login.path} className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50">
                    Войти
                  </Link>
                )}
              </div>
            </div>
          </div>
        </DialogPanel>
      </Dialog>
    </header>
  );
};

export default Header;
