'use client';
import cx from 'classnames';
import { ITagColor } from '@/app/lib/definitions';

const getColor = (color: ITagColor) => {
  switch (color) {
    case 'orange':
      return 'border-orange-600 bg-orange-200 text-orange-600';
    case 'blue':
      return 'border-blue-600 bg-blue-200 text-blue-600';
    case 'green':
      return 'border-green-600 bg-green-200 text-green-600';
    case 'red':
      return 'border-red-600 bg-red-200 text-red-600';
    case 'white':
      return 'border-gray-300 bg-white text-gray-900';
    case 'gray':
      return 'border-gray-300 bg-gray-300 text-gray-900';
    case 'yellow':
    default:
      return 'border-yellow-600 bg-yellow-200 text-yellow-600';
  }
};

interface ITagProps {
  text: string;
  color: ITagColor;
  onClick?: () => void;
}

const Tag = ({ text, color, onClick }: ITagProps) => {
  return (
    <button
      onClick={() => onClick?.()}
      type="button"
      className={cx('h-[32px] w-max whitespace-nowrap rounded-md px-2 py-1 text-sm border', getColor(color), {
        'pointer-events-none': !onClick,
      })}
    >
      {text}
    </button>
  );
};

export default Tag;
