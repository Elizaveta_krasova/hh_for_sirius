'use client';
import { SessionProvider } from 'next-auth/react';
import React, { ReactNode } from 'react';
import { AppSession } from '@/app/lib/definitions';

interface Props {
  children: ReactNode;
  session: AppSession | null;
}
function Provider({ children, session }: Props) {
  return <SessionProvider session={session}>{children}</SessionProvider>;
}

export default Provider;
