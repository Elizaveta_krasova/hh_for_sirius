import React, { FC } from 'react';
import cx from 'classnames';

interface ITextFieldProps {
  value: string | number | undefined;
  onChange: (value: string | number) => void;
  label: string;
  name: string;
  multiline?: boolean;
  error?: boolean;
  type?: 'email' | 'number' | 'text';
}

const TextField: FC<ITextFieldProps> = ({ error, name, type = 'text', value, onChange, label, multiline }) => {
  return (
    <div>
      <label htmlFor={label} className={cx('block text-sm font-medium leading-6 text-gray-900', { '!text-red-500': error })}>
        {label}
      </label>
      <div className="mt-2">
        {multiline ? (
          <textarea
            rows={5}
            name={name}
            id={name}
            value={value}
            onChange={(e) => onChange(e.target.value)}
            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-blue-600 sm:text-sm sm:leading-6"
          />
        ) : (
          <input
            multiple
            type={type}
            name={name}
            id={name}
            value={value}
            onChange={(e) => onChange(e.target.value)}
            className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-blue-600 sm:text-sm sm:leading-6"
          />
        )}
      </div>
    </div>
  );
};
export default TextField;
