import React, { FC } from 'react';
import cx from 'classnames';
import { Switch } from '@headlessui/react';

interface ITextFieldProps {
  value: boolean;
  onChange: (value: boolean) => void;
  label: string;
}

const Toggle: FC<ITextFieldProps> = ({ value, onChange, label }) => {
  return (
    <div>
      <label htmlFor={label} className="block text-sm font-medium leading-6 text-gray-900">
        {label}
      </label>
      <div className="mt-2">
        <Switch
          checked={value}
          onChange={onChange}
          className={cx(
            value ? 'bg-blue-600' : 'bg-gray-200',
            'relative inline-flex h-6 w-11 flex-shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus:ring-2 focus:ring-blue-600 focus:ring-offset-2'
          )}
        >
          <span className="sr-only">Use setting</span>
          <span
            aria-hidden="true"
            className={cx(
              value ? 'translate-x-5' : 'translate-x-0',
              'pointer-events-none inline-block h-5 w-5 transform rounded-full bg-white shadow ring-0 transition duration-200 ease-in-out'
            )}
          />
        </Switch>
      </div>
    </div>
  );
};
export default Toggle;
