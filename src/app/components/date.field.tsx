import React, { FC } from 'react';
import cx from 'classnames';

interface IDateFieldProps {
  value: string | Date | undefined;
  onChange: (value: string) => void;
  label: string;
  name: string;
  error?: boolean;
}

const DateField: FC<IDateFieldProps> = ({ error, value, onChange, label, name }) => {
  const inputValue = value ? new Date(value)?.toISOString().split('T')[0] : '';

  return (
    <div>
      <label htmlFor={label} className={cx('block text-sm font-medium leading-6 text-gray-900', { '!text-red-500': error })}>
        {label}
      </label>
      <div className="mt-2">
        <input
          type="date"
          name={name}
          id={label}
          value={inputValue}
          onChange={(e) => onChange(e.target.value)}
          className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
        />
      </div>
    </div>
  );
};
export default DateField;
