import React from 'react';

import Card from '@/app/resumes/card';
import { getResumes } from '@/app/lib/data';
import { Resume } from '@/app/lib/definitions';

export default async function Page() {
  const resumes: Resume[] = await new Promise((resolve) => {
    setTimeout(() => {
      resolve(getResumes());
    }, 5000);
  });

  return (
    <div className="flex flex-col gap-4">
      <div role="list" className="p-6 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {resumes.map((resume) => (
          <Card resume={resume} key={resume.id} className="col-span-1" />
        ))}
      </div>
    </div>
  );
}
