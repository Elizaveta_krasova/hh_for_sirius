'use client';
import { Tag } from '@/app/lib/definitions';
import React from 'react';
import TagUi from '@/app/components/tag';

const About = ({ tags }: { tags: Tag[] }) => {
  return (
    <div className="flex flex-col gap-4">
      <div className="px-4 sm:px-0">
        <h3 className="text-lg font-semibold leading-7 text-gray-900">Теги</h3>
      </div>
      {!!tags?.length && (
        <div className="border-t border-gray-100">
          <dl className="divide-y divide-gray-100">
            <div className="px-4 py-4 sm:px-0">
              <div className="mt-1 sm:mt-0 flex gap-4">
                {tags.map((tag) => (
                  <TagUi text={tag.title} key={tag.id} color="gray" />
                ))}
              </div>
            </div>
          </dl>
        </div>
      )}
    </div>
  );
};
export default About;
