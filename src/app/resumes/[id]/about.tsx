'use client';
import React from 'react';

const About = ({ about }: { about: string }) => {
  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-semibold leading-7 text-gray-900">О себе</h3>
        </div>
        {about && (
          <div className="border-t border-gray-100">
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:px-0">
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{about ?? ''}</dd>
              </div>
            </dl>
          </div>
        )}
      </div>
    </>
  );
};
export default About;
