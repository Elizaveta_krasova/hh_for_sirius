import React from 'react';
import { getGroupById, getResumeById, getStudentByUserId } from '@/app/lib/data';
import Projects from '@/app/resumes/[id]/projects';
import Info from '@/app/resumes/[id]/info';
import About from '@/app/resumes/[id]/about';
import Tags from '@/app/resumes/[id]/tags';
import Group from '@/app/resumes/[id]/group';
import BackButton from '@/app/components/back.button';

interface IPageProps {
  params: { id: string };
}

export default async function Page({ params }: IPageProps) {
  const resume = await getResumeById(params?.id as string);
  const student = await getStudentByUserId(resume.user_id as string);
  const group = await getGroupById(student?.group_id);
  return (
    <div className="h-full overflow-y-auto">
      <div className="mx-auto max-w-[900px] mt-6">
        <BackButton />
      </div>
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Info student={student} />
        {!!resume.projects?.length && <Projects projects={resume.projects ?? []} />}
        <About about={resume.about} />
        <Tags tags={resume.tags} />
        <Group group={group} />
      </div>
    </div>
  );
}
