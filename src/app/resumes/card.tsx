'use client';
import React, { FC } from 'react';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import { Resume } from '@/app/lib/definitions';
import { PropsWithClassName } from '@/app/types/common';
import cx from 'classnames';

interface ICardProps {
  resume: Resume;
}

const Card: FC<PropsWithClassName<ICardProps>> = ({ resume, className }) => {
  return (
    <Link
      href={routes.resumes.resume.generatePath(resume.id)}
      type="button"
      className={cx('bg-white relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center', className)}
    >
      <div className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start">
        <p className="font-bold text-xl text-gray-900">{resume.about}</p>
        <p className="font-semibold text-lg text-gray-700 leading-6">{`${resume.name} ${resume.lastname}`}</p>
      </div>
    </Link>
  );
};

export default Card;
