import React from 'react';

import Card from '@/app/tags/card';
import { getTags } from '@/app/lib/data';
import AddTagButton from '@/app/tags/add.tag.button';

export default async function Page() {
  const tags = await getTags();
  return (
    <div className="flex flex-col gap-4">
      <div className="flex justify-end p-6 pb-0">
        <div>
          <AddTagButton />
        </div>
      </div>
      <div role="list" className="p-6 flex flex-col gap-4">
        {tags.map((tag) => (
          <Card tag={tag} key={tag.id} />
        ))}
      </div>
    </div>
  );
}
