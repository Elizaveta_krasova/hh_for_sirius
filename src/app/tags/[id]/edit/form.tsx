'use client';
import React from 'react';
import TextField from '@/app/components/text.field';
import DateField from '@/app/components/date.field';
import Button from '@/app/components/button';
import { useRouter } from 'next/navigation';
import { routes } from '@/app/config/routes';
import { useFormik } from 'formik';
import { updateDirectionAction, updateGroupAction, updateTagAction, updateVacancyAction } from '@/app/lib/actions';
import { Direction, EVacancyType, Group, Tag, Vacancy } from '@/app/lib/definitions';
import SelectBox from '@/app/components/select.box';

export function Form({ tag }: { tag: Tag }) {
  const router = useRouter();

  const formik = useFormik<Tag>({
    initialValues: {
      ...tag,
    },
    validate: (values: Tag) => {
      const errors: any = {};
      if (!values.title) errors.title = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await updateTagAction({ ...values });
        router.push(routes.tags.path);
      } catch (e) {}
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="space-y-12">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-base font-semibold leading-7 text-gray-900">Тег</h2>
          <p className="mt-1 text-sm leading-6 text-gray-600">Редактирование тега</p>

          <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-10">
            <div className="sm:col-span-6">
              <TextField
                error={!!formik.errors.title}
                name="title"
                value={formik.values.title}
                onChange={(value) => formik.setFieldValue('title', value)}
                label="title"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-6 flex items-center justify-end gap-x-6">
        <Button text="Отмена" color="white" onClick={() => router.push(routes.tags.path)} />

        <Button type="submit" text="Сохранить" color="green" />
      </div>
    </form>
  );
}
