import { getTagById } from '@/app/lib/data';
import { Form } from '@/app/tags/[id]/edit/form';

export default async function Page({ params }: { params: { id: string } }) {
  const tag = await getTagById(params.id);

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form tag={tag} />
      </div>
    </div>
  );
}
