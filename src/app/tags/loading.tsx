import Spinner from '@/app/components/spinner';

export default function Loading() {
  return (
    <div className="relative mx-auto my-10 w-min">
      <Spinner />
    </div>
  );
}
