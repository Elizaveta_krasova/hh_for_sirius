'use client';
import React, { FC } from 'react';
import Button from '@/app/components/button';
import Link from 'next/link';
import { routes } from '@/app/config/routes';

const AddTagButton: FC = () => {
  return (
    <>
      <Link href={routes.tags.create.path}>
        <Button text="Добавить тег" color="green" />
      </Link>
    </>
  );
};

export default AddTagButton;
