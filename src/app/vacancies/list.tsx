import React, { Suspense, useState } from 'react';
import Card from '@/app/vacancies/card';
import { getServerSession } from '@/app/lib/server-utils';
import { getVacancies } from '@/app/lib/data';
import { EUserRole } from '@/app/lib/definitions';
import SkeletionCard from './skeleton.card';

const List = async () => {
  let vacancies = await getVacancies();

  const session = await getServerSession();
  if (session?.user.role === EUserRole.COMPANY) {
    vacancies = vacancies.filter((v) => v.company_id === session?.user.company?.id);
  }

  return (
    <div className="flex flex-col gap-4 w-full p-6 ">
      <div role="list" className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {vacancies.map((vacancy) => (
          <Suspense key={vacancy.id} fallback={<SkeletionCard />}>
            <Card id={vacancy.id} hideApply className="col-span-1" />
          </Suspense>
        ))}
      </div>
    </div>
  );
};

export default List;
