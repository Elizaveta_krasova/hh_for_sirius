'use client';
import React, { FC, SyntheticEvent, useState } from 'react';
import Button from '@/app/components/button';
import { createApplyAction } from '@/app/lib/actions';
import useUser from '@/app/hooks/use.user';
import ApplyModal from '@/app/applies/apply.modal';
import { Vacancy } from '@/app/lib/definitions';

interface IApplyButtonProps {
  vacancy: Vacancy;
}

const ApplyButton: FC<IApplyButtonProps> = ({ vacancy }) => {
  const { isStudent, studentId, studentResumeId } = useUser();
  const [isShowModal, setIsShowModal] = useState(false);

  const apply = async (e: SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();

    if (vacancy.task) {
      setIsShowModal(true);
      return;
    }
    try {
      await createApplyAction({ vacancy_id: vacancy.id, student_id: studentId });
    } catch (e) {}
  };

  if (!isStudent) return null;

  return (
    <>
      <div className="flex justify-end">
        <Button disabled={!studentResumeId} text={!studentResumeId ? 'Заполните резюме для отклика' : 'Откликнуться'} color="blue" onClick={apply} />
      </div>
      {isShowModal && <ApplyModal task={vacancy.task as string} open={isShowModal} vacancyId={vacancy.id} close={() => setIsShowModal(false)} />}
    </>
  );
};

export default ApplyButton;
