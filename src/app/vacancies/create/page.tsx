import { Form } from '@/app/vacancies/create/form';
import { getServerSession } from '@/app/lib/server-utils';
import { getTagsAction } from '@/app/lib/actions';

export default async function Page() {
  const session = await getServerSession();
  const tags = await getTagsAction();
  const companyId = session?.user?.company?.id as string;

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form companyId={companyId} tags={tags} />
      </div>
    </div>
  );
}
