'use client';
import React from 'react';
import TextField from '@/app/components/text.field';
import DateField from '@/app/components/date.field';
import TagUi from '@/app/components/tag';
import Button from '@/app/components/button';
import { useRouter } from 'next/navigation';
import { routes } from '@/app/config/routes';
import { useFormik } from 'formik';
import SelectBox from '@/app/components/select.box';
import { createVacancyAction } from '@/app/lib/actions';
import { EVacancyType, IVacancyCreate, Tag } from '@/app/lib/definitions';
import { Label } from '@headlessui/react';
import cx from 'classnames';

export function Form({ companyId, tags }: { companyId: string; tags: Tag[] }) {
  const router = useRouter();

  const formik = useFormik<IVacancyCreate>({
    initialValues: {
      title: '',
      description: '',
      type: EVacancyType.PRACTICE,
      date_from: undefined,
      date_to: undefined,
      expiration: undefined,
      task: '',
      city: '',
      is_remote: false,
      tags: [],
    },
    validate: (values: IVacancyCreate) => {
      const errors: any = {};
      if (!values.title) errors.title = true;
      if (!values.description) errors.description = true;
      if (!values.type) errors.type = true;
      if (!values.date_to) errors.date_to = true;
      if (!values.date_from) errors.date_from = true;
      if (!values.expiration) errors.expiration = true;
      if (!values.city) errors.city = true;
      //
      // if (!values.email) {
      //   errors.email = 'Required';
      // } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      //   errors.email = 'Invalid email address';
      // }
      return errors;
    },
    onSubmit: async (values) => {
      try {
        const id = await createVacancyAction({ ...values, company_id: companyId });
        router.push(routes.vacancies.vacancy.generatePath(id));
      } catch (e) {}
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="space-y-12">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-base font-semibold leading-7 text-gray-900">Вакансия</h2>
          <p className="mt-1 text-sm leading-6 text-gray-600">Создание вакансии</p>

          <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-10">
            <div className="sm:col-span-5">
              <TextField
                error={!!formik.errors.title}
                name="title"
                value={formik.values.title}
                onChange={(value) => formik.setFieldValue('title', value)}
                label="Название"
              />
            </div>

            <div className="sm:col-span-5">
              <SelectBox
                items={[
                  { value: EVacancyType.JOB, text: 'Работа' },
                  { value: EVacancyType.INTERNSHIP, text: 'Интернaтура' },
                  { value: EVacancyType.PRACTICE, text: 'Практика' },
                ]}
                error={!!formik.errors.type}
                value={formik.values.type}
                onChange={(value) => formik.setFieldValue('type', value)}
                label="Тип Вакансии"
              />
            </div>

            <div className="sm:col-span-10">
              <TextField
                error={!!formik.errors.description}
                name="description"
                value={formik.values.description}
                onChange={(value) => formik.setFieldValue('description', value)}
                label="Описание"
                multiline
              />
            </div>

            <div className="sm:col-span-5">
              <DateField
                error={!!formik.errors.date_to}
                name="date_to"
                value={formik.values.date_to}
                onChange={(value) => formik.setFieldValue('date_to', value)}
                label="Date To"
              />
            </div>
            <div className="sm:col-span-5">
              <DateField
                error={!!formik.errors.date_from}
                name="date_from"
                value={formik.values.date_from}
                onChange={(value) => formik.setFieldValue('date_from', value)}
                label="Date From"
              />
            </div>

            <div className="sm:col-span-5">
              <DateField
                error={!!formik.errors.expiration}
                name="expiration"
                value={formik.values.expiration}
                onChange={(value) => formik.setFieldValue('expiration', value)}
                label="Expiration"
              />
            </div>

            <div className="sm:col-span-10">
              <TextField name="task" value={formik.values.task} onChange={(value) => formik.setFieldValue('task', value)} label="Задание" multiline />
            </div>

            <div className="sm:col-span-5">
              <TextField
                error={!!formik.errors.city}
                name="city"
                value={formik.values.city}
                onChange={(value) => formik.setFieldValue('city', value)}
                label="Город"
              />
            </div>
            <div className="sm:col-span-5">
              <SelectBox
                value={!!formik.values.is_remote}
                items={[
                  { value: true, text: 'Да' },
                  { value: false, text: 'Нет' },
                ]}
                onChange={(value) => formik.setFieldValue('is_remote', value)}
                label="Удаленка"
              />
            </div>
            <div className="sm:col-span-10">
              <div>
                <label className={cx('block text-sm font-medium leading-6 text-gray-900')}>Теги</label>
                <div className="mt-2 flex gap-4">
                  {tags.map((tag) => (
                    <TagUi
                      text={tag.title}
                      color={formik.values.tags?.find((t) => t.id === tag.id) ? 'gray' : 'white'}
                      onClick={() => {
                        formik.setFieldValue(
                          'tags',
                          formik.values.tags?.find((t) => t.id === tag.id)
                            ? formik.values.tags?.filter((t) => t.id !== tag.id)
                            : [...(formik.values.tags ?? []), tag]
                        );
                      }}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-6 flex items-center justify-end gap-x-6">
        <Button text="Отмена" color="white" onClick={() => router.push(routes.vacancies.path)} />

        <Button type="submit" text="Сохранить" color="green" />
      </div>
    </form>
  );
}
