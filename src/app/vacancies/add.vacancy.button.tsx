'use client';
import React, { FC } from 'react';
import Button from '@/app/components/button';
import Link from 'next/link';
import { routes } from '@/app/config/routes';

const AddVacancyButton: FC = () => {
  return (
    <>
      <Link href={routes.vacancies.create.path}>
        <Button text="Добавить вакансию" color="green" />
      </Link>
    </>
  );
};

export default AddVacancyButton;
