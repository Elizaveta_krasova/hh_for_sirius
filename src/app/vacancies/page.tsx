import React from 'react';

import { getServerSession } from '@/app/lib/server-utils';
import AddVacancyButton from '@/app/vacancies/add.vacancy.button';

import { EUserRole } from '@/app/lib/definitions';
import List from '@/app/vacancies/list';

export default async function Page() {
  const session = await getServerSession();

  return (
    <div className="flex flex-col gap-4">
      {session?.user.role === EUserRole.COMPANY && (
        <div className="flex justify-end p-6 pb-0">
          <div>
            <AddVacancyButton />
          </div>
        </div>
      )}
      <List />
    </div>
  );
}
