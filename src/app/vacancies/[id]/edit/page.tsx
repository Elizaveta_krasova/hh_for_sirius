import { getVacancyById } from '@/app/lib/data';
import { Form } from '@/app/vacancies/[id]/edit/form';
import { getTagsAction } from '@/app/lib/actions';
import React from 'react';

export default async function Page({ params }: { params: { id: string } }) {
  const vacancy = await getVacancyById(params.id);
  const tags = await getTagsAction();

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form vacancy={vacancy} tags={tags} />
      </div>
    </div>
  );
}
