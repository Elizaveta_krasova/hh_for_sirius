import React from 'react';
import CompanyCard from '@/app/companies/card';
import { getApplyByVacancyId, getCompanyById, getVacancyById } from '@/app/lib/data';
import Link from 'next/link';
import Button from '@/app/components/button';
import { EUserRole } from '@/app/lib/definitions';
import { getServerSession } from '@/app/lib/server-utils';
import { routes } from '@/app/config/routes';
import ApplyButton from '@/app/vacancies/apply.button';
import cx from 'classnames';
import TagUi from '@/app/components/tag';
import { getTagByVacancyIdAction, getTagsAction } from '@/app/lib/actions';
import { redirect } from 'next/navigation';
import BackButton from '@/app/components/back.button';

interface IPageProps {
  params: { id: string };
}

export default async function Page({ params }: IPageProps) {
  const session = await getServerSession();
  const vacancy = await getVacancyById(params.id);
  const company = await getCompanyById(vacancy.company_id);
  const applies = await getApplyByVacancyId(vacancy.id);
  const tags = await getTagByVacancyIdAction(vacancy.id);

  const isMyCompany = session?.user.company?.id === company.id;
  const isStudentApplies = applies.find((a) => a.student_id === session?.user?.student?.id);

  return (
    <div className="h-full overflow-y-auto">
      <div className="mx-auto max-w-[900px] mt-6">
        <BackButton />
      </div>
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <div className="flex flex-col gap-4">
          <div className="flex flex-col md:flex-row gap-4 justify-between">
            <div className="px-4 sm:px-0">
              <h3 className="text-lg font-semibold leading-7 text-gray-900">Информация</h3>
              <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Личные данные</p>
              <div className="flex flex-col md:flex-row gap-4 my-1">
                {session?.user.role === EUserRole.COMPANY && isMyCompany && (
                  <Link href={routes.vacancies.vacancy.edit.generatePath(vacancy.id)}>
                    <Button text="Редактировать" color="blue" />
                  </Link>
                )}
                {session?.user.role === EUserRole.COMPANY && isMyCompany && !!applies.length && (
                  <Link href={routes.applies.vacancy.generatePath(vacancy.id)}>
                    <Button text={`Просмотреть отклики (${applies.length})`} color="green" />
                  </Link>
                )}
                {session?.user.role === EUserRole.COMPANY && isMyCompany && !applies.length && <Button disabled text="Нет новых откликов" color="green" />}
                {session?.user.role === EUserRole.STUDENT && !isStudentApplies && session.user.student?.is_approved && <ApplyButton vacancy={vacancy} />}
                {session?.user.role === EUserRole.STUDENT && isStudentApplies && <Button disabled text="Уже откликнулись" color="green" />}
                {session?.user.company?.id !== company.id && (
                  <Link href={routes.companies.company.generatePath(company.id)}>
                    <Button text="Просмотреть компанию" color="blue" />
                  </Link>
                )}
              </div>
            </div>
          </div>
          <div className="border-t border-gray-100">
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Название</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.title}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Описание</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.description}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Тип</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.type}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Город</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.city}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Задание</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.task}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Date To</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.date_to.toLocaleString()}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Date From</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.date_from.toLocaleString()}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Expiration</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.expiration.toLocaleString()}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Удаленно</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{vacancy.is_remote ? 'Да' : 'Нет'}</dd>
              </div>
            </dl>
          </div>

          {!!tags?.length && (
            <div>
              <label className={cx('block text-sm font-medium leading-6 text-gray-900')}>Теги</label>
              <div className="mt-2 flex gap-4">
                {tags.map((tag) => (
                  <TagUi text={tag.title} color="gray" />
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
