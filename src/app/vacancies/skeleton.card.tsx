'use client';
import React, { FC } from 'react';
import { PropsWithClassName } from '@/app/types/common';
import cx from 'classnames';

interface ICardProps {
  hideApply?: boolean;
}

const SkeletionCard: FC<PropsWithClassName<ICardProps>> = ({ hideApply = false, className }) => {
  return (
    <>
      <div
        className={cx(
          'animate-pulse h-[322px] bg-white w-full min-w-[300px] relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center',
          className
        )}
      >
        <div className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start">
          <p className="h-[20px] bg-slate-200 rounded-full w-48 mb-4"></p>
          <p className="h-[20px] bg-slate-200 font-semibold text-lg text-gray-700 leading-6"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
          <p className="bg-slate-200 h-[20px]"></p>
        </div>
      </div>
    </>
  );
};

export default SkeletionCard;
