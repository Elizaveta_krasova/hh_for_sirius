import React, { FC } from 'react';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import { PropsWithClassName } from '@/app/types/common';
import cx from 'classnames';
import ApplyButton from '@/app/vacancies/apply.button';
import { getVacancyById } from '../lib/data';

interface ICardProps {
  id: string;
  hideApply?: boolean;
}

const Card: FC<PropsWithClassName<ICardProps>> = async ({ hideApply = false, id, className }) => {
  const vacancy = await getVacancyById(id);

  // await new Promise((r) => setTimeout(r, Math.random() * 5000));

  return (
    <>
      <Link
        href={routes.vacancies.vacancy.generatePath(vacancy.id)}
        type="button"
        className={cx(
          'bg-white w-full min-w-[300px] relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center',
          className
        )}
      >
        <div className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start">
          <p className="font-bold text-xl text-gray-900">{vacancy.title}</p>
          <p className="font-semibold text-lg text-gray-700 leading-6">{vacancy.description}</p>
          <p>Тип: {vacancy.type}</p>
          <p>Город: {vacancy.city}</p>
          <p>Задание: {!!vacancy.task ? 'Есть' : 'Нет'}</p>
          <p>Date To: {new Date(vacancy.date_to)?.toJSON()?.split('T')?.[0] ?? ''}</p>
          <p>Date From: {new Date(vacancy.date_from)?.toJSON()?.split('T')?.[0] ?? ''}</p>
          <p>Expiration: {new Date(vacancy.expiration)?.toJSON()?.split('T')?.[0] ?? ''}</p>
          <p>Удаленка: {vacancy.is_remote ? 'Yes' : 'No'}</p>
          {!hideApply && <ApplyButton vacancy={vacancy} />}
        </div>
      </Link>
    </>
  );
};

export default Card;
