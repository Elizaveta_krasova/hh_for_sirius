'use client';
import React, { FC } from 'react';
import { Dialog, DialogPanel, DialogTitle, Transition, TransitionChild } from '@headlessui/react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { useFormik } from 'formik';
import { createResumeAction } from '@/app/lib/actions';
import { routes } from '@/app/config/routes';
import { useRouter } from 'next/navigation';

interface ICreateAboutModalProps {
  open: boolean;
  studentId: string;
  close: () => void;
}

const CreateAboutModal: FC<ICreateAboutModalProps> = ({ open: receivedOpen, close, studentId }) => {
  const router = useRouter();
  const formik = useFormik<{ about: string }>({
    initialValues: { about: '' },
    validate: (values: { about: string }) => {
      const errors: any = {};
      if (!values.about) errors.about = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await createResumeAction(studentId, values.about);
        router.refresh();
        close();
      } catch (e) {}
    },
  });

  return (
    <Transition show={receivedOpen}>
      <Dialog className="relative z-10" onClose={close}>
        <TransitionChild
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center">
            <TransitionChild
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <DialogPanel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full max-w-[700px] sm:p-6">
                <div className="flex flex-col gap-4">
                  <DialogTitle className="font-semibold">Редактирование Личных данных</DialogTitle>
                  <div className="max-h-[500px] overflow-y-auto">
                    <form onSubmit={formik.handleSubmit}>
                      <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">
                          <TextField
                            error={!!formik.errors.about}
                            name="about"
                            value={formik.values.about}
                            onChange={(value) => formik.setFieldValue('about', value)}
                            label="О себе"
                            multiline
                          />
                        </div>
                      </div>

                      <div className="mt-6 flex items-center justify-end gap-x-6">
                        <Button text="Отмена" color="white" onClick={() => close()} />

                        <Button type="submit" text="Сохранить" color="green" />
                      </div>
                    </form>
                  </div>
                </div>
              </DialogPanel>
            </TransitionChild>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default CreateAboutModal;
