'use client';
import React, { FC } from 'react';
import { Dialog, DialogPanel, DialogTitle, Transition, TransitionChild } from '@headlessui/react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { IStudentStatus, Student } from '@/app/lib/definitions';
import { useFormik } from 'formik';
import { updateStudentAction } from '@/app/lib/actions';
import SelectBox from '@/app/components/select.box';
import DateField from '@/app/components/date.field';
import { routes } from '@/app/config/routes';
import { useRouter } from 'next/navigation';

interface IEditInfoModalProps {
  open: boolean;
  student: Student;
  close: () => void;
}

const EditInfoModal: FC<IEditInfoModalProps> = ({ open: receivedOpen, close, student }) => {
  const router = useRouter();
  const formik = useFormik<Student>({
    initialValues: { ...student },
    validate: (values: Student) => {
      const errors: any = {};
      if (!values.lastname) errors.lastname = true;
      if (!values.middle_name) errors.middle_name = true;
      if (!values.name) errors.name = true;
      if (!values.birthday) errors.birthday = true;
      if (!values.link_to_git) errors.link_to_git = true;
      if (!values.phone) errors.phone = true;
      if (!values.telegram) errors.telegram = true;
      if (!values.status) errors.status = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await updateStudentAction({ ...values });
        router.refresh();
        close();
      } catch (e) {}
    },
  });

  return (
    <Transition show={receivedOpen}>
      <Dialog className="relative z-10" onClose={close}>
        <TransitionChild
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center">
            <TransitionChild
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <DialogPanel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full max-w-[700px] sm:p-6">
                <div className="flex flex-col gap-4">
                  <DialogTitle className="font-semibold">Редактирование Личных данных</DialogTitle>
                  <div className="max-h-[500px] overflow-y-auto">
                    <form onSubmit={formik.handleSubmit}>
                      <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">
                          <div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-9">
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.name}
                                name="name"
                                value={formik.values.name}
                                onChange={(value) => formik.setFieldValue('name', value)}
                                label="Имя"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.lastname}
                                name="lastname"
                                value={formik.values.lastname}
                                onChange={(value) => formik.setFieldValue('lastname', value)}
                                label="Фамилия"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.middle_name}
                                name="middle_name"
                                value={formik.values.middle_name}
                                onChange={(value) => formik.setFieldValue('middle_name', value)}
                                label="Отчество"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <DateField
                                error={!!formik.errors.birthday}
                                name="birthday"
                                value={formik.values.birthday}
                                onChange={(value) => formik.setFieldValue('birthday', value)}
                                label="День Рождения"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <SelectBox
                                items={[
                                  { value: IStudentStatus.job, text: 'Работа' },
                                  { value: IStudentStatus.INTERN, text: 'Интернaтура' },
                                  { value: IStudentStatus.PRACTICE, text: 'Практика' },
                                  { value: IStudentStatus.NOT_LOOKING_FOR_JOB, text: 'Не ищу работу' },
                                ]}
                                error={!!formik.errors.status}
                                value={formik.values.status}
                                onChange={(value) => formik.setFieldValue('status', value)}
                                label="Статус"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.phone}
                                name="phone"
                                value={formik.values.phone}
                                onChange={(value) => formik.setFieldValue('phone', value)}
                                label="Телефон"
                              />
                            </div>
                            <div className="sm:col-span-6">
                              <TextField
                                error={!!formik.errors.link_to_git}
                                name="link_to_git"
                                value={formik.values.link_to_git}
                                onChange={(value) => formik.setFieldValue('link_to_git', value)}
                                label="Ссылка Git"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <TextField
                                name="telegram"
                                value={formik.values.telegram}
                                onChange={(value) => formik.setFieldValue('telegram', value)}
                                label="Телеграмм"
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="mt-6 flex items-center justify-end gap-x-6">
                        <Button text="Отмена" color="white" onClick={() => close()} />

                        <Button type="submit" text="Сохранить" color="green" />
                      </div>
                    </form>
                  </div>
                </div>
              </DialogPanel>
            </TransitionChild>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default EditInfoModal;
