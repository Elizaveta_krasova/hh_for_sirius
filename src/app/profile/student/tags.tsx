'use client';
import { Student, Tag } from '@/app/lib/definitions';
import React, { useState } from 'react';
import EditAboutModal from '@/app/profile/student/edit.about.modal';
import CreateAboutModal from '@/app/profile/student/create.about.modal';
import TagUi from '@/app/components/tag';
import EditTagsModal from '@/app/profile/student/edit.tags.modal';

const About = ({ student, tags, allTags }: { student: Student; tags: Tag[]; allTags: Tag[] }) => {
  const [isShowEditModal, setIsShowEditModal] = useState(false);

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-semibold leading-7 text-gray-900">Теги</h3>
          <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Отображаемые теги в вашем резюме</p>
          <button className="mt-1 text-sm leading-6 text-blue-500 hover:underline" onClick={() => setIsShowEditModal(true)}>
            Редактировать
          </button>
        </div>
        {!!tags?.length && (
          <div className="border-t border-gray-100">
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:px-0">
                <div className="mt-1 sm:mt-0 flex gap-4">
                  {tags.map((tag) => (
                    <TagUi text={tag.title} key={tag.id} color="gray" />
                  ))}
                </div>
              </div>
            </dl>
          </div>
        )}
      </div>
      {isShowEditModal && <EditTagsModal tags={tags} allTags={allTags} studentId={student.id} close={() => setIsShowEditModal(false)} open={isShowEditModal} />}
    </>
  );
};
export default About;
