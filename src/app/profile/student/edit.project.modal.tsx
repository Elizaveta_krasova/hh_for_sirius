'use client';
import React, { FC } from 'react';
import { Dialog, DialogPanel, DialogTitle, Transition, TransitionChild } from '@headlessui/react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { Project } from '@/app/lib/definitions';
import { useFormik } from 'formik';
import { updateProjectAction } from '@/app/lib/actions';
import DateField from '@/app/components/date.field';
import { routes } from '@/app/config/routes';
import { useRouter } from 'next/navigation';

interface IEditProjectModalProps {
  open: boolean;
  project: Project;
  close: () => void;
}

const EditProjectModal: FC<IEditProjectModalProps> = ({ open: receivedOpen, close, project }) => {
  const router = useRouter();
  const formik = useFormik<Project>({
    initialValues: { ...project },
    validate: (values: Project) => {
      const errors: any = {};
      if (!values.date_from) errors.date_from = true;
      if (!values.date_to) errors.date_to = true;
      if (!values.title) errors.title = true;
      if (!values.description) errors.description = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await updateProjectAction(values);
        router.refresh();
        close();
      } catch (e) {}
    },
  });

  return (
    <Transition show={receivedOpen}>
      <Dialog className="relative z-10" onClose={close}>
        <TransitionChild
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center">
            <TransitionChild
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <DialogPanel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full max-w-[700px] sm:p-6">
                <div className="flex flex-col gap-4">
                  <DialogTitle className="font-semibold">Добавление проекта</DialogTitle>
                  <div className="max-h-[500px] overflow-y-auto">
                    <form onSubmit={formik.handleSubmit}>
                      <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">
                          <div className="grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-9">
                            <div className="sm:col-span-3">
                              <TextField
                                error={!!formik.errors.title}
                                name="title"
                                value={formik.values.title}
                                onChange={(value) => formik.setFieldValue('title', value)}
                                label="Название"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <DateField
                                error={!!formik.errors.date_to}
                                name="date_to"
                                value={formik.values.date_to}
                                onChange={(value) => formik.setFieldValue('date_to', value)}
                                label="Date To"
                              />
                            </div>
                            <div className="sm:col-span-3">
                              <DateField
                                error={!!formik.errors.date_from}
                                name="date_from"
                                value={formik.values.date_from}
                                onChange={(value) => formik.setFieldValue('date_from', value)}
                                label="Date From"
                              />
                            </div>
                            <div className="sm:col-span-9">
                              <TextField
                                error={!!formik.errors.description}
                                name="description"
                                value={formik.values.description}
                                onChange={(value) => formik.setFieldValue('description', value)}
                                label="Описание"
                                multiline
                              />
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="mt-6 flex items-center justify-end gap-x-6">
                        <Button text="Отмена" color="white" onClick={() => close()} />

                        <Button type="submit" text="Сохранить" color="green" />
                      </div>
                    </form>
                  </div>
                </div>
              </DialogPanel>
            </TransitionChild>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default EditProjectModal;
