import { getServerSession } from '@/app/lib/server-utils';
import Info from '@/app/profile/student/info';
import React from 'react';
import Projects from '@/app/profile/student/projects';
import { getGroupById, getResumeByStudentId, getStudentByUserId, getTags } from '@/app/lib/data';
import About from '@/app/profile/student/about';
import Tags from '@/app/profile/student/tags';
import Group from '@/app/profile/student/group';

export default async function Page() {
  const session = await getServerSession();
  const student = await getStudentByUserId(session?.user.id as string);
  const resume = await getResumeByStudentId(session?.user?.student?.id as string);
  const allTags = await getTags();
  const group = student?.group_id ? await getGroupById(student?.group_id) : null;

  return (
    // <div className="bg-white py-6 h-full overflow-y-auto">
    //   <div className="flex flex-col gap-8 mx-auto max-w-[900px] ">
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Info student={student} />
        {!!resume.projects?.length && <Projects resumeId={resume?.id} projects={resume.projects ?? []} />}
        <About student={student} about={resume.about} resumeId={resume.id} />
        <Tags student={student} tags={resume.tags ?? []} allTags={allTags} />
        {group && <Group group={group} />}
      </div>
    </div>
  );
}
