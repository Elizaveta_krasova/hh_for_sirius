'use client';
import { Project } from '@/app/lib/definitions';
import React, { useState } from 'react';
import CreateProjectModal from '@/app/profile/student/create.project.modal';
import EditProjectModal from '@/app/profile/student/edit.project.modal';
import { deleteProjectAction } from '@/app/lib/actions';
import { useRouter } from 'next/navigation';

const Projects = ({ projects, resumeId }: { projects: Project[]; resumeId: string }) => {
  const router = useRouter();
  const [isShowEditModal, setIsShowEditModal] = useState(false);
  const [editProject, setEditProject] = useState<Project | null>(null);
  const [isShowCreateModal, setIsShowCreateModal] = useState(false);

  const deleteProject = async (projectId: string) => {
    try {
      await deleteProjectAction(projectId);
      router.refresh();
    } catch (e) {}
  };

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-semibold leading-7 text-gray-900">Проекты</h3>
          <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Ваши проекты</p>
          <button className="mt-1 text-sm leading-6 text-blue-500 hover:underline" onClick={() => setIsShowCreateModal(true)}>
            Добавить
          </button>
        </div>
        {projects.map((p, index) => (
          <div className="border-t border-gray-100" key={p.id}>
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">{index + 1}</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0 flex gap-4">
                  <button
                    className="mt-1 text-sm leading-6 text-blue-500 hover:underline"
                    onClick={() => {
                      setEditProject(p);
                      setIsShowEditModal(true);
                    }}
                  >
                    Редактировать
                  </button>
                  <button className="mt-1 text-sm leading-6 text-red-500 hover:underline" onClick={() => deleteProject(p.id)}>
                    Удалить
                  </button>
                </dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Название</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{p.title}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Описание</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{p.description}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Date From</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{new Date(p.date_from)?.toLocaleDateString()}</dd>
              </div>
              <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                <dt className="text-sm font-medium leading-6 text-gray-900">Date To</dt>
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{new Date(p.date_to)?.toLocaleDateString()}</dd>
              </div>
            </dl>
          </div>
        ))}
      </div>
      {isShowCreateModal && <CreateProjectModal open={isShowCreateModal} resumeId={resumeId} close={() => setIsShowCreateModal(false)} />}
      {isShowEditModal && editProject && (
        <EditProjectModal
          open={isShowEditModal}
          project={editProject}
          close={() => {
            setEditProject(null);
            setIsShowEditModal(false);
          }}
        />
      )}
    </>
  );
};
export default Projects;
