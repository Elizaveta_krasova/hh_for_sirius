'use client';
import { Student } from '@/app/lib/definitions';
import React, { useState } from 'react';
import EditAboutModal from '@/app/profile/student/edit.about.modal';
import CreateAboutModal from '@/app/profile/student/create.about.modal';

const About = ({ student, about, resumeId }: { student: Student; about: string; resumeId: string }) => {
  const [isShowEditModal, setIsShowEditModal] = useState(false);
  const [isShowCreateModal, setIsShowCreateModal] = useState(false);

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="px-4 sm:px-0">
          <h3 className="text-lg font-semibold leading-7 text-gray-900">О себе</h3>
          <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Расскажите про себя</p>
          <button className="mt-1 text-sm leading-6 text-blue-500 hover:underline" onClick={() => setIsShowEditModal(true)}>
            Редактировать
          </button>
        </div>
        {about && (
          <div className="border-t border-gray-100">
            <dl className="divide-y divide-gray-100">
              <div className="px-4 py-4 sm:px-0">
                <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{about ?? ''}</dd>
              </div>
            </dl>
          </div>
        )}
      </div>

      {isShowEditModal && <EditAboutModal about={about} close={() => setIsShowEditModal(false)} open={isShowEditModal} resumeId={resumeId} />}
      {isShowCreateModal && <CreateAboutModal close={() => setIsShowCreateModal(false)} open={isShowCreateModal} studentId={student?.id} />}
    </>
  );
};
export default About;
