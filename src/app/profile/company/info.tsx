'use client';
import { Company } from '@/app/lib/definitions';
import React, { useState } from 'react';
import EditInfoModal from '@/app/profile/company/edit.info.modal';

const Info = ({ company }: { company: Company }) => {
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="flex flex-col md:flex-row gap-4 justify-between">
          <div className="px-4 sm:px-0">
            <h3 className="text-lg font-semibold leading-7 text-gray-900">Информация Компании</h3>
            <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">Личные данные</p>
            <button className="mt-1 text-sm leading-6 text-blue-500 hover:underline" onClick={() => setIsShowModal(true)}>
              Редактировать
            </button>
          </div>
        </div>
        <div className="border-t border-gray-100">
          <dl className="divide-y divide-gray-100">
            <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <dt className="text-sm font-medium leading-6 text-gray-900">Название</dt>
              <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.title}</dd>
            </div>
            <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <dt className="text-sm font-medium leading-6 text-gray-900">Направление</dt>
              <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.industry}</dd>
            </div>
            <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <dt className="text-sm font-medium leading-6 text-gray-900">Адрес</dt>
              <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.address}</dd>
            </div>
            <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <dt className="text-sm font-medium leading-6 text-gray-900">Ссылка</dt>
              <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">{company.link_to_web_site}</dd>
            </div>
          </dl>
        </div>
      </div>
      {isShowModal && <EditInfoModal open={isShowModal} close={() => setIsShowModal(false)} company={company} />}
    </>
  );
};
export default Info;
