import React from 'react';
import Info from '@/app/profile/company/info';
import { getCompanyById } from '@/app/lib/data';
import { getServerSession } from '@/app/lib/server-utils';

export default async function Page() {
  const session = await getServerSession();
  const company = await getCompanyById(session?.user?.company?.id as string);

  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Info company={company} />
      </div>
    </div>
  );
}
