import React from 'react';

import Card from '@/app/applies/card';
import { getApplyByVacancyId } from '@/app/lib/data';
import { getServerSession } from '@/app/lib/server-utils';
import BackButton from '@/app/components/back.button';

export default async function Page({ params }: { params: { id: string } }) {
  const session = await getServerSession();
  const companyId = session?.user?.company?.id as string;
  const applies = await getApplyByVacancyId(params.id);

  return (
    <div className="flex flex-col gap-4">
      <div className="mx-auto w-full mt-6 px-6">
        <BackButton />
      </div>
      <div role="list" className="p-6 flex flex-col gap-4">
        {applies.map((apply) => !!apply && <Card apply={apply} key={apply.id} className="col-span-1" />)}
      </div>
    </div>
  );
}
