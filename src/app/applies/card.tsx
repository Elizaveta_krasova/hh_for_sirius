'use client';
import React, { FC, SyntheticEvent } from 'react';
import { Apply } from '@/app/lib/definitions';
import { PropsWithClassName } from '@/app/types/common';
import cx from 'classnames';
import Button from '@/app/components/button';
import useUser from '@/app/hooks/use.user';
import { Disclosure, DisclosurePanel } from '@headlessui/react';
import { useRouter } from 'next/navigation';
import { routes } from '@/app/config/routes';
import { getResumeByStudentIdAction, updateApplyAction } from '@/app/lib/actions';

interface ICardProps {
  apply: Apply;
}

const Card: FC<PropsWithClassName<ICardProps>> = ({ apply, className }) => {
  const { isStudent, studentId } = useUser();
  const router = useRouter();

  const goToResume = async (e: SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      const resume = await getResumeByStudentIdAction(apply.student_id);
      if (resume.id) {
        router.push(routes.resumes.resume.generatePath(resume.id));
      } else {
        console.log('нет резюме у тестового студента');
      }
    } catch (e) {}
  };

  const onApply = async (e: SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      await updateApplyAction(apply.id, apply.vacancy_id, true);
    } catch (e) {}
  };

  const onDiscard = async (e: SyntheticEvent) => {
    e.preventDefault();
    e.stopPropagation();
    try {
      await updateApplyAction(apply.id, apply.vacancy_id, false);
    } catch (e) {}
  };

  const isNew = apply.is_applied === null;
  const isApplied = apply.is_applied;
  const isDiscard = !apply.is_applied && !isNew;

  return (
    <div
      className={cx('bg-white w-full min-w-[300px] relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 items-center', className)}
    >
      <Disclosure as="div" className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start" defaultOpen={false}>
        <Disclosure.Button>
          <div className="flex flex-col items-start gap-2">
            <p className="font-bold text-xl text-gray-900">Вакансия: {apply.title}</p>
            <div className="flex items-center gap-2 font-medium text-md text-gray-900">
              Статус:
              <span className="inline-flex items-center gap-x-1.5 rounded-md px-2 py-1 text-xs font-medium text-gray-900 ring-1 ring-inset ring-gray-200">
                <svg
                  className={cx('h-1.5 w-1.5', {
                    'fill-green-500': isApplied,
                    'fill-red-500': isDiscard,
                    'fill-orange-500': isNew,
                  })}
                  viewBox="0 0 6 6"
                  aria-hidden="true"
                >
                  <circle cx={3} cy={3} r={3} />
                </svg>
                <span className="font-medium"> {isApplied ? 'Подтверждено' : isNew ? 'Новое' : 'Отклонено'}</span>
              </span>
            </div>
            <p className="font-medium text-md text-gray-900">Средний балл студента: {apply.average_score}</p>
            <p className="font-medium text-md text-gray-900">Количество проектов студента: {apply.count_project}</p>
            <div className="flex gap-3 flex-wrap">
              <div className="mt-2">
                <Button text="Открыть резюме студента" color="blue" onClick={goToResume} />
              </div>
              <>
                {(isApplied || isNew) && (
                  <div className="mt-2">
                    <Button text="Отклонить" color="red" onClick={onDiscard} />
                  </div>
                )}
                {(isDiscard || isNew) && (
                  <div className="mt-2">
                    <Button text="Подтвердить" color="green" onClick={onApply} />
                  </div>
                )}
              </>
            </div>
            <span className="text-blue-500 text-md hover:underline mt-2">Нажмите, чтобы посмотреть решение задания</span>
          </div>
        </Disclosure.Button>
        <DisclosurePanel>
          <p className="font-medium text-md text-gray-900">Решение:</p>
          {apply.task}
        </DisclosurePanel>
        {/*<p className="font-semibold text-lg text-gray-700 leading-6">{vacancy.description}</p>*/}
        {/*<p>Тип: {vacancy.type}</p>*/}
        {/*<p>Город: {vacancy.city}</p>*/}
        {/*<p>Задание: {!!vacancy.task ? 'Есть' : 'Нет'}</p>*/}
        {/*<p>Date To: {String(vacancy.date_to)}</p>*/}
        {/*<p>Date From: {String(vacancy.date_from)}</p>*/}
        {/*<p>Expiration: {String(vacancy.expiration)}</p>*/}
        {/*<p>Удаленка: {vacancy.is_remote ? 'Yes' : 'No'}</p>*/}
        {/*{isStudent && (*/}
        {/*  <div className="flex justify-end">*/}
        {/*    <Button text="Откликнуться" color="blue" onClick={apply} />*/}
        {/*  </div>*/}
        {/*)}*/}
      </Disclosure>
    </div>
  );
};

export default Card;
