'use client';
import React, { FC } from 'react';
import { Dialog, DialogPanel, DialogTitle, Transition, TransitionChild } from '@headlessui/react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { IApplyCreate } from '@/app/lib/definitions';
import { useFormik } from 'formik';
import { createApplyAction } from '@/app/lib/actions';
import useUser from '@/app/hooks/use.user';

interface IApplyModalProps {
  open: boolean;
  vacancyId: string;
  task: string;
  close: () => void;
}

const ApplyModal: FC<IApplyModalProps> = ({ open: receivedOpen, close, vacancyId, task }) => {
  const { studentId } = useUser();
  const formik = useFormik<Pick<IApplyCreate, 'task'>>({
    initialValues: {
      task: '',
    },
    validate: (values: Pick<IApplyCreate, 'task'>) => {
      const errors: any = {};
      if (!values.task) errors.task = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await createApplyAction({ ...values, vacancy_id: vacancyId, student_id: studentId });
        close();
      } catch (e) {}
    },
  });

  return (
    <Transition show={receivedOpen}>
      <Dialog className="relative z-10" onClose={close}>
        <TransitionChild
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </TransitionChild>

        <div className="fixed inset-0 z-10 w-screen overflow-y-auto">
          <div className="flex min-h-full items-end justify-center p-4 text-center sm:items-center">
            <TransitionChild
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <DialogPanel className="relative transform overflow-hidden rounded-lg bg-white px-4 pb-4 pt-5 text-left shadow-xl transition-all sm:my-8 w-full max-w-sm sm:p-6">
                <div className="flex flex-col gap-4">
                  <DialogTitle className="font-semibold">Отклик</DialogTitle>
                  <div className="flex flex-col gap-4 max-h-[500px] overflow-y-auto">
                    <form onSubmit={formik.handleSubmit}>
                      <div className="space-y-12">
                        <div className="border-b border-gray-900/10 pb-12">
                          <p className="mt-1 text-sm leading-6 text-gray-600">У этой вакансии имеется задание, отправьте его решение для отклика.</p>
                          <p className="mt-1 text-sm leading-6 text-gray-600 mb-2">
                            <span className="text-base font-semibold leading-7 text-gray-900">Задание: </span>
                            {task}
                          </p>

                          <TextField
                            error={!!formik.errors.task}
                            name="task"
                            value={formik.values.task}
                            onChange={(value) => formik.setFieldValue('task', value)}
                            label="Решение"
                            multiline
                          />
                        </div>
                      </div>

                      <div className="mt-6 flex items-center justify-end gap-x-6">
                        <Button text="Отмена" color="white" onClick={close} />

                        <Button type="submit" text="Сохранить" color="green" />
                      </div>
                    </form>
                  </div>
                </div>
              </DialogPanel>
            </TransitionChild>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default ApplyModal;
