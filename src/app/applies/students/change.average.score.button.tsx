'use client';
import React, { FC, useState } from 'react';
import Button from '@/app/components/button';
import ChangeGroupModal from '@/app/applies/students/change.group.modal';
import { Group } from '@/app/lib/definitions';
import ChangeAverageScoreModal from '@/app/applies/students/change.average.score.modal';

const ChangeAverageScoreButton: FC<{ currentAverageScore: number; studentId: string }> = ({ currentAverageScore, studentId }) => {
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <Button text="Сменить средний балл" color="blue" onClick={() => setIsShowModal(true)} />
      <ChangeAverageScoreModal currentAverageScore={currentAverageScore} open={isShowModal} studentId={studentId} close={() => setIsShowModal(false)} />
    </>
  );
};

export default ChangeAverageScoreButton;
