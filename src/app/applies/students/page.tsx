import React from 'react';

import Card from '@/app/applies/students/card';
import { getGroups, getStudents, getStudentsNotApproved } from '@/app/lib/data';
export default async function Page() {
  const studentsNotApproved = await getStudentsNotApproved();
  const allStudents = await getStudents();
  const groups = await getGroups();

  return (
    <div className="flex flex-col gap-4 p-6 ">
      <div className="px-4 sm:px-0">
        <h3 className="text-lg font-semibold leading-7 text-gray-900">Неподтвержденные студенты</h3>
      </div>

      <div role="list" className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {studentsNotApproved.map((student) => (
          <Card student={student} key={student.id} currentGroup={student.group_id} groups={groups} className="col-span-1" />
        ))}
      </div>

      <div className="px-4 sm:px-0">
        <h3 className="text-lg font-semibold leading-7 text-gray-900">Все студенты</h3>
      </div>

      <div role="list" className="w-full grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        {allStudents.map((student) => (
          <Card student={student} key={student.id} className="col-span-1" groups={groups} currentGroup={student.group_id} />
        ))}
      </div>
    </div>
  );
}
