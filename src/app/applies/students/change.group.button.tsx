'use client';
import React, { FC, useState } from 'react';
import Button from '@/app/components/button';
import ChangeGroupModal from '@/app/applies/students/change.group.modal';
import { Group } from '@/app/lib/definitions';

const ChangeGroupButton: FC<{ groups?: Group[]; currentGroup: string; studentId: string }> = ({ groups, currentGroup, studentId }) => {
  const [isShowModal, setIsShowModal] = useState(false);

  return (
    <>
      <Button text="Сменить группу" color="blue" onClick={() => setIsShowModal(true)} />
      <ChangeGroupModal groups={groups} currentGroup={currentGroup} open={isShowModal} studentId={studentId} close={() => setIsShowModal(false)} />
    </>
  );
};

export default ChangeGroupButton;
