'use client';
import React, { FC } from 'react';
import { Group, Student } from '@/app/lib/definitions';
import { PropsWithClassName } from '@/app/types/common';
import cx from 'classnames';
import Button from '@/app/components/button';
import useUser from '@/app/hooks/use.user';
import { useRouter } from 'next/navigation';
import { updateStudentApprovedAction } from '@/app/lib/actions';
import ChangeGroupButton from '@/app/applies/students/change.group.button';
import ChangeAverageScoreButton from '@/app/applies/students/change.average.score.button';

interface ICardProps {
  student: Student;
  currentGroup: string;
  groups?: Group[];
}
const statusesLabel = {
  practice: 'Практика',
  internship: 'Интерн',
  job: 'Работа',
  not_looking_FOR_JOB: 'Не ищу работу',
};
const Card: FC<PropsWithClassName<ICardProps>> = ({ student, className, groups, currentGroup }) => {
  const { isAdmin } = useUser();
  const router = useRouter();

  const onApply = async () => {
    try {
      await updateStudentApprovedAction(student.id, true);
      router.refresh();
    } catch (e) {}
  };

  const onDiscard = async () => {
    try {
      await updateStudentApprovedAction(student.id, false);
      router.refresh();
    } catch (e) {}
  };

  const isNew = student.is_approved === null;
  const isApplied = student.is_approved;
  const isDiscard = !student.is_approved && !isNew;

  return (
    <div
      className={cx('bg-white relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center', className)}
    >
      <div className="flex flex-col gap-2 text-sm leading-5 text-gray-500 w-full self-start">
        <p className="font-bold text-xl text-gray-900">Имя: {student.name}</p>
        <p>Фамилия: {student.lastname}</p>
        <p>Отчество: {student.middle_name}</p>
        <p>Статус: {statusesLabel[student.status]}</p>
        <p>Телеграмм: {student.telegram}</p>
        <p>Телефон: {student.phone}</p>
        <p>Ссылка: {student.link_to_git}</p>
        <p>Средний балл: {student.average_score}</p>
        <p>Группа: {groups?.find((g) => g.id === currentGroup)?.direction_title || 'Не установлена'}</p>
        {(isNew || isApplied) && <Button text="Отклонить" color="red" onClick={onDiscard} />}
        {(isNew || isDiscard) && <Button text="Подтвердить" color="green" onClick={onApply} />}
        {isAdmin && <ChangeGroupButton groups={groups} currentGroup={currentGroup} studentId={student.id} />}
        {isAdmin && <ChangeAverageScoreButton currentAverageScore={student.average_score} studentId={student.id} />}
      </div>
    </div>
  );
};

export default Card;
