export const routes = {
  root: {
    path: '/',
  },
  vacancies: {
    path: '/vacancies',
    vacancy: {
      generatePath: (vacancyId: number | string) => `/vacancies/${vacancyId}`,
      edit: {
        generatePath: (vacancyId: number | string) => `/vacancies/${vacancyId}/edit`,
      },
    },
    create: {
      path: '/vacancies/create',
    },
  },
  login: {
    path: '/login',
  },
  register: {
    path: '/register',
  },
  resumes: {
    path: '/resumes',
    resume: {
      generatePath: (resumeId: number | string) => `/resumes/${resumeId}`,
    },
    create: {
      path: '/resumes/create',
    },
  },
  companies: {
    path: '/companies',
    company: {
      generatePath: (companyId: number | string) => `/companies/${companyId}`,
    },
  },
  directions: {
    path: '/directions',
    direction: {
      edit: {
        generatePath: (directionId: number | string) => `/directions/${directionId}/edit`,
      },
    },
    create: {
      path: '/directions/create',
    },
  },
  groups: {
    path: '/groups',
    group: {
      generatePath: (groupId: number | string) => `/groups/${groupId}`,
      edit: {
        generatePath: (groupId: number | string) => `/groups/${groupId}/edit`,
      },
    },
    create: {
      path: '/groups/create',
    },
  },
  tags: {
    path: '/tags',
    tag: {
      generatePath: (tagId: number | string) => `/tags/${tagId}`,
      edit: {
        generatePath: (tagId: number | string) => `/tags/${tagId}/edit`,
      },
    },
    create: {
      path: '/tags/create',
    },
  },
  applies: {
    path: '/applies',
    vacancy: {
      generatePath: (vacancyId: number | string) => `/applies/vacancy/${vacancyId}`,
    },
    students: {
      path: '/applies/students',
    },
  },
  profile: {
    student: {
      path: '/profile/student',
      resume: {
        path: '/profile/student/resume',
      },
    },
    company: {
      path: '/profile/company',
    },
    admin: {
      path: '/profile/student',
    },
  },
};

export const protectedRoutesUrls = [
  routes.tags.path,
  routes.applies.path,
  routes.groups.path,
  routes.vacancies.path,
  routes.directions.path,
  routes.companies.path,
  routes.resumes.path,
  routes.profile.student.path,
  routes.profile.company.path,
  routes.profile.admin.path,
];
