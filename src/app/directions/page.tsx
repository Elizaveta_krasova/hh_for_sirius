import React from 'react';

import Card from '@/app/directions/card';
import { getDirections } from '@/app/lib/data';
import { getServerSession } from '@/app/lib/server-utils';
import { EUserRole } from '@/app/lib/definitions';
import AddDirectionButton from '@/app/directions/add.direction.button';

export default async function Page() {
  const session = await getServerSession();

  const directions = await getDirections();

  return (
    <div className="flex flex-col gap-4">
      {session?.user.role === EUserRole.ADMIN && (
        <div className="flex justify-end p-6 pb-0">
          <div>
            <AddDirectionButton />
          </div>
        </div>
      )}
      <div role="list" className="p-6 flex flex-col gap-4">
        {directions.map((direction) => (
          <Card direction={direction} key={direction.id} />
        ))}
      </div>
    </div>
  );
}
