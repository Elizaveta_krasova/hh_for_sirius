'use client';
import React, { FC } from 'react';
import Link from 'next/link';
import { routes } from '@/app/config/routes';
import Button from '@/app/components/button';
import { Direction } from '@/app/lib/definitions';
import useUser from '@/app/hooks/use.user';
import { deleteDirectionAction } from '@/app/lib/actions';

interface ICardProps {
  direction: Direction;
}

const Card: FC<ICardProps> = ({ direction }) => {
  const { isAdmin } = useUser();

  return (
    <div className="bg-white relative flex justify-between gap-x-6 py-5 border border-gray-200 rounded-lg px-4 hover:border-gray-700 items-center">
      <div className="flex flex-col gap-2">
        <div className="flex min-w-0 gap-x-4">
          <div className="min-w-0 flex-auto">
            <p className="mt-1 flex text-xs font-semibold leading-6 text-gray-900">
              <span className="relative truncate">{direction.title}</span>
            </p>
            <p className="mt-1 flex text-xs leading-5 text-gray-500">
              <span className="relative truncate">{direction.description}</span>
            </p>
          </div>
        </div>
      </div>
      {isAdmin && (
        <div className="flex gap-4">
          <Link href={routes.directions.direction.edit.generatePath(direction.id)}>
            <Button text="Редактировать" color="blue" />
          </Link>
          <Button
            text="Удалить"
            color="red"
            onClick={async () => {
              await deleteDirectionAction(direction.id);
            }}
          />
        </div>
      )}
    </div>
  );
};

export default Card;
