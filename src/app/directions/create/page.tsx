import { Form } from '@/app/directions/create/form';
import React from 'react';

export default async function Page() {
  return (
    <div className="h-full overflow-y-auto">
      <div className="my-6 px-4 py-6 sm:px-6 overflow-hidden bg-white shadow sm:rounded-lg flex flex-col gap-8 mx-auto max-w-[900px]">
        <Form />
      </div>
    </div>
  );
}
