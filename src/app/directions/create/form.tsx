'use client';
import React from 'react';
import TextField from '@/app/components/text.field';
import Button from '@/app/components/button';
import { useRouter } from 'next/navigation';
import { routes } from '@/app/config/routes';
import { useFormik } from 'formik';
import { createDirectionAction } from '@/app/lib/actions';
import { IDirectionCreate } from '@/app/lib/definitions';

export function Form() {
  const router = useRouter();

  const formik = useFormik<IDirectionCreate>({
    initialValues: {
      title: '',
      description: '',
    },
    validate: (values: IDirectionCreate) => {
      const errors: any = {};
      if (!values.title) errors.title = true;
      if (!values.description) errors.description = true;
      return errors;
    },
    onSubmit: async (values) => {
      try {
        await createDirectionAction({ ...values });
        router.replace(routes.directions.path);
      } catch (e) {}
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="space-y-12">
        <div className="border-b border-gray-900/10 pb-12">
          <h2 className="text-base font-semibold leading-7 text-gray-900">Направление</h2>
          <p className="mt-1 text-sm leading-6 text-gray-600">Создание направления</p>

          <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-10">
            <div className="sm:col-span-5">
              <TextField
                error={!!formik.errors.title}
                name="title"
                value={formik.values.title}
                onChange={(value) => formik.setFieldValue('title', value)}
                label="Название"
              />
            </div>

            <div className="sm:col-span-10">
              <TextField
                error={!!formik.errors.description}
                name="description"
                value={formik.values.description}
                onChange={(value) => formik.setFieldValue('description', value)}
                label="Описание"
                multiline
              />
            </div>
          </div>
        </div>
      </div>

      <div className="mt-6 flex items-center justify-end gap-x-6">
        <Button text="Отмена" color="white" onClick={() => router.push(routes.directions.path)} />

        <Button type="submit" text="Сохранить" color="green" />
      </div>
    </form>
  );
}
