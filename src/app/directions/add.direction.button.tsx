'use client';
import React, { FC } from 'react';
import Button from '@/app/components/button';
import Link from 'next/link';
import { routes } from '@/app/config/routes';

const AddDirectionButton: FC = () => {
  return (
    <>
      <Link href={routes.directions.create.path} >
        <Button text="Добавить направление" color="green" />
      </Link>
    </>
  );
};

export default AddDirectionButton;
