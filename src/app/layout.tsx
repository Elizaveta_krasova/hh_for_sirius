import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import cx from 'classnames';
import './globals.css';
import Header from '@/app/components/header';
import Provider from '@/app/components/session.provider';
import { getServerSession } from '@/app/lib/server-utils';
import Spinner from '@/app/components/spinner';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'HH Sirius',
  description: 'HH Sirius',
};

export default async function RootLayout(
  props: Readonly<{
    children: React.ReactNode;
  }>
) {
  const session = await getServerSession();

  return (
    <html lang="en">
      <Provider session={session}>
        <body className={cx(inter.className)}>
          <Header isAuth={!!session?.user} />
          <div className="root h-full bg-gray-100">{props.children}</div>
        </body>
      </Provider>
    </html>
  );
}
