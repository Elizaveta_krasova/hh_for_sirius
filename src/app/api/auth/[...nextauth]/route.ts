import type { NextAuthOptions } from 'next-auth';
import NextAuth from 'next-auth';

import CredentialsProvider from 'next-auth/providers/credentials';
import { routes } from '@/app/config/routes';
import { getCompanyById, getCompanyByUserId, getStudentByUserId, getUserByEmail } from '@/app/lib/data';
import bcrypt from 'bcrypt';
import { EUserRole } from '@/app/lib/definitions';

// документация:
// https://next-auth.js.org/getting-started/example

export const AuthOptions: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: 'Credentials',
      credentials: {
        email: { label: 'email', type: 'text', placeholder: 'email' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials, req) {
        const email = credentials?.email;
        const password = credentials?.password;

        if (!email || !password) return null;

        const user = await getUserByEmail(email);

        if (!user) return null;
        let userStudent = {};
        if (user.role === EUserRole.STUDENT) {
          userStudent = await getStudentByUserId(user.id);
        }
        let userCompany = {};
        if (user.role === EUserRole.COMPANY) {
          userCompany = await getCompanyByUserId(user.id);
        }
        const passwordsMatch = await bcrypt.compare(password, user.password_hash);

        if (passwordsMatch) {
          if (user.role === EUserRole.STUDENT) {
            return { ...user, student: userStudent };
          }
          if (user.role === EUserRole.COMPANY) {
            return { ...user, company: userCompany };
          }
          return user;
        }

        return null;
      },
    }),
  ],
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async jwt({ token, user }) {
      const userByEmail = await getUserByEmail(token.email!);
      return { ...token, user_id: userByEmail.id, user_role: userByEmail.role, ...user };
    },
    async session({ session, token }) {
      return {
        ...session,
        user: {
          ...session.user,
          student: token.student ? { ...token.student } : undefined,
          company: token.company ? { ...token.company } : undefined,
          id: token.user_id,
          role: token.user_role,
        },
      };
    },
    async redirect({ url, baseUrl }) {
      return routes.vacancies.path;
    },
  },
  pages: {
    signIn: '/login',
  },
};

const handler = NextAuth(AuthOptions);

export { handler as GET, handler as POST };
