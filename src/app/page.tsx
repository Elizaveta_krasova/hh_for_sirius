import icon from '../../public/sirus.jpeg';
const features = [
  {
    name: 'Сокращенное наименование',
    description: 'Научно-технологический университет «Сириус», Университет «Сириус», НТУ «Сириус» / на английском языке - Sirius University',
  },
  { name: 'Учредитель', description: 'Образовательный Фонд «Талант и успех» (ОГРН 1147700000172)' },
  { name: 'Адрес', description: '354340, Российская Федерация, Краснодарский край, федеральная территория «Сириус», Олимпийский пр., д.1' },
  { name: 'Телефон', description: '8 (800) 100 41 55' },
  { name: 'E-mail', description: 'info@siriusuniversity.ru' },
];

export default function Home() {
  return (
    <main className="flex flex-col items-center justify-between">
      <div className="bg-white w-full">
        <div aria-hidden="true" className="relative">
          <img src={icon.src} alt="" className="h-96 w-full object-cover object-center" />
          <div className="absolute inset-0 bg-gradient-to-t from-white" />
        </div>

        <div className="relative mx-auto -mt-12 max-w-7xl px-4 pb-16 sm:px-6 sm:pb-24 lg:px-8">
          <div className="mx-auto max-w-2xl text-center lg:max-w-4xl">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Университет «Сириус»</h2>
            <p className="mt-4 text-gray-500">Это новый подход к образованию и научно-исследовательской деятельности.</p>
            <p className="mt-4 text-gray-500">
              В нем нет привычных факультетов и кафедр, ядро университета составляют Научные центры по приоритетным для России направлениям, которые возглавляют
              ученые с мировым именем. Благодаря этому университет быстро готовит востребованных специалистов с актуальными компетенциями.
            </p>
          </div>

          <dl className="mx-auto mt-16 grid max-w-2xl grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 sm:gap-y-16 lg:max-w-none lg:grid-cols-3 lg:gap-x-8">
            {features.map((feature) => (
              <div key={feature.name} className="border-t border-gray-200 pt-4">
                <dt className="font-medium text-gray-900">{feature.name}</dt>
                <dd className="mt-2 text-sm text-gray-500">{feature.description}</dd>
              </div>
            ))}
          </dl>
        </div>
      </div>
    </main>
  );
}
